//npm modules
import { AuthGuard } from "@nestjs/passport";

export class OptionalJwtAuthGuard extends AuthGuard() {

    // Override handleRequest so it never throws an error
    handleRequest(_err, user, _info, _context) {
        return user;
    }

}