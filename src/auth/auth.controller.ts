//npm modules
import { Controller, Post, Body, ValidationPipe, Get, UseGuards, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

//local modules
import { SignUpDTO } from './dto/signup.dto';
import { LoginDTO } from './dto/login.dto';
import { TwoFactorDTO } from './dto/twofactor.dto';
import { AuthService } from './auth.service';
import { ReturnSigninInterface, TwoFactorEnabledReturn, ReturnUserInterface } from './dto/interfaces';
import {RequestPasswordResetDTO as EmailDTO} from './dto/requestPasswordReset.dto';
import { GetUser } from './get-user.decorator';
import { ResetPasswordDTO } from './dto/resetPassword.dto';
import { UserDocument } from './user.schema';
import { EditUserDTO } from './dto/edit-user.dto';
import { TokenOrRejectDTO } from './dto/token-or-reject.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ResetTokenDTO } from './dto/resettoken.dto';
import { PhoneDTO } from './dto/phone.dto';
import { OptionalJwtAuthGuard } from './optional-auth.guard';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService){}

    @UseGuards(AuthGuard())
    @Get('/')
    async autoLogin (@GetUser() user: UserDocument): Promise<ReturnUserInterface> {
        
        return {
            _id: user._id,
            email: user.email,
            shoppingCart: user.shoppingCart,
            phonenumber: user.phonenumber,
            twoFactorAuth: user.twoFactorAuth,
            countryCode: user.countryCode,
            phoneCountryCode: user.phoneCountryCode,
            firstName: user.firstName,
            lastName: user.lastName
        };
    }

    @Get('/getuserwithaddress')
    @UseGuards(AuthGuard())
    getUserWithAddress (@GetUser() user: UserDocument): Promise<ReturnUserInterface> {

        return this.authService.getUserWithAddress(user);
    }

    @Post('/')
    login (@Body(ValidationPipe) loginDTO: LoginDTO): Promise<ReturnSigninInterface | TwoFactorEnabledReturn> {
        
        return this.authService.login(loginDTO);
    }

    @Post('/twofactorauth')
    twoFactorAuth (@Body(ValidationPipe) twoFactorDTO: TwoFactorDTO): Promise<ReturnSigninInterface> {

        return this.authService.twoFactorAuth(twoFactorDTO);
    }

    @Post('/signup')
    signup (@Body(ValidationPipe) signupDTO: SignUpDTO): Promise<SuccessfulOPDTO> {
        return this.authService.signup(signupDTO);
    }

    @Post('/requestpasswordreset')
    requestPasswordReset (@Body(ValidationPipe) requestPasswordResetDTO: EmailDTO): Promise<string> {

        const {email} = requestPasswordResetDTO;
        return this.authService.requestPasswordReset(email);
    }

    @Post('/sendsms')
    requestSms (@Body('token') token: string): Promise<SuccessfulOPDTO> {
        return this.authService.sendSms(token);
    }

    @Post('/validateresettoken')
    validateResetToken (@Body(ValidationPipe) resetTokenDTO: ResetTokenDTO): Promise<SuccessfulOPDTO> {
        return this.authService.validateResetToken(resetTokenDTO);
    }

    @Post('/resetpassword')
    resetPassword (@Body(ValidationPipe) resetPasswordDTO: ResetPasswordDTO): Promise<SuccessfulOPDTO> {
        return this.authService.resetPassword(resetPasswordDTO);
    }

    @Post('/confirmemailupdate')
    confirmOrRejectEmailUpdate (@Body(ValidationPipe) tokenOrRejectDTO: TokenOrRejectDTO): 
    Promise<SuccessfulOPDTO> {

        return this.authService.confirmOrRejectNewEmail(tokenOrRejectDTO);
    }

    @UseGuards(AuthGuard())
    @Put('/edituser')
    editUser (@Body(ValidationPipe) editUserDTO: EditUserDTO, @GetUser() user: UserDocument): Promise<ReturnUserInterface> {
        
        return this.authService.editUser(editUserDTO, user);
    }

    @Post('/checkemail')
    @UseGuards(OptionalJwtAuthGuard)
    checkEmail (@Body(ValidationPipe) emailDTO: EmailDTO,
    @GetUser({ignoreError: true}) user: UserDocument): Promise<SuccessfulOPDTO> {

        const { email } = emailDTO;

        return this.authService.checkEmail(email, user);
    }

    @Post('/checkphone')
    @UseGuards(OptionalJwtAuthGuard)
    checkPhone (@Body(ValidationPipe) phoneDTO: PhoneDTO, 
    @GetUser({ignoreError: true}) user: UserDocument): Promise<SuccessfulOPDTO> {

        return this.authService.checkPhone(phoneDTO, user);
    }
}
