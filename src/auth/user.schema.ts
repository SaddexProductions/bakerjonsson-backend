//npm modules
import {Document, SchemaTypes} from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

//local modules
import { CartItem } from './dto/interfaces';
import { ConversationDocument } from 'src/contact/conversation.schema';

const {ObjectId} = SchemaTypes;

export type UserDocument = User & Document;

@Schema()
export class User {

    @Prop({required: true, type: String})
    email: string;

    @Prop({
        type: {
            email: {
            type: String
            },
            newEmailToken: {
                type: String
            }
        }
    })
    newEmail: {
        email: string;
        newEmailToken: string;
    }

    @Prop({required: true, type: String})
    password: string;

    @Prop({required: true, minlength: 2, maxlength: 2, type: String})
    countryCode: string;

    @Prop({required: true, minlength: 2, maxlength: 2, type: String})
    phoneCountryCode: string;

    @Prop({required: true, minlength: 4, maxlength: 50, type: String})
    phonenumber: string;

    @Prop({required: true, default: false, type: Boolean})
    twoFactorAuth: boolean;

    @Prop({type: String})
    authyID: string;

    @Prop({required: true, type: String, minlength: 2, maxlength: 20})
    firstName: string;

    @Prop({required: true, type: String, minlength: 2, maxlength: 20})
    lastName: string;

    @Prop({type: String, required: false})
    validResetToken: string;

    @Prop({required: true, maxlength: 400, type: String})
    address: string;

    @Prop({required: true, default: false, type: Boolean})
    isAdmin: boolean;

    @Prop({
        type: [{
            count: Number,
            _id: {
                type: ObjectId,
                ref: "Product"
            }
        }]
    })
    shoppingCart: CartItem[];

    @Prop([{
        type: ObjectId,
        ref: "Conversation"
    }])
    conversations?: string[] | ConversationDocument[];

    @Prop([{
        type: ObjectId,
        ref: "Order"
    }])
    orders?: string[];

    @Prop([{
        type: ObjectId,
        ref: "Review"
    }])
    reviews?: string[];

}

export const UserSchema = SchemaFactory.createForClass(User);