//npm modules
import { createParamDecorator, ExecutionContext, ForbiddenException } from "@nestjs/common";

//local modules
import { UserDocument } from "./user.schema";
import { ResolvedTokenDTO } from "./dto/resolvedToken.dto";
import { TokenEnum } from "./dto/token.enum";

export const GetUser = createParamDecorator((getUserOptions: {adminRequired?: boolean; ignoreError?: boolean}, 
    ctx: ExecutionContext): UserDocument => {

    let adminRequired;
    let ignoreError;
    
    if (getUserOptions) {
        adminRequired = getUserOptions.adminRequired;
        ignoreError = getUserOptions.ignoreError;
    }

    const req = ctx.switchToHttp().getRequest();

    const contents: ResolvedTokenDTO = req.user;

    if (!ignoreError && contents.type !== TokenEnum.LOGIN) throw new ForbiddenException("This token can't be used for signing in");

    if (!adminRequired && !contents.user && !ignoreError && !contents.user.isAdmin) 
        throw new ForbiddenException("You lack the required user rights to perform this operation");

    if (!contents) return null;

    return contents.user;
});