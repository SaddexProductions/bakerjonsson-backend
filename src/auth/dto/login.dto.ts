//npm modules
import { IsString, IsEmail, MaxLength, IsOptional, IsJWT } from "class-validator";

export class LoginDTO {

    @IsString()
    @IsEmail()
    @MaxLength(200)
    email: string;

    @IsString()
    password: string;

    @IsString()
    @IsOptional()
    @IsJWT()
    deviceToken?: string;
}