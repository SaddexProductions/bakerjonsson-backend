//npm modules
import 
{
    IsString, 
    MinLength, 
    MaxLength,
    IsOptional,
    Matches,
    IsBoolean
} from 'class-validator';

//local modules
import { LoginDTO } from './login.dto';
import { countryRegex , passwordRegex, nameRegex } from '../miscConstants';

export class SignUpDTO extends LoginDTO {

    @IsString()
    @MinLength(8)
    @MaxLength(25)
    @Matches(passwordRegex, {message: 'Password too weak'})
    password: string;

    @IsString()
    @MinLength(2)
    @Matches(countryRegex)
    countryCode: string;

    @IsString()
    @MinLength(2)
    @IsOptional()
    @Matches(countryRegex)
    phoneCountryCode?: string;

    @IsString()
    @Matches(/[0-9]{0,14}$/)
    phone: string;

    @IsString()
    @MinLength(2)
    @MaxLength(20)
    @Matches(nameRegex)
    firstName: string;

    @IsString()
    @MinLength(2)
    @MaxLength(20)
    @Matches(nameRegex)
    lastName: string;

    @IsString()
    @MinLength(4)
    @MaxLength(200)
    street: string;

    @IsString()
    @MaxLength(50)
    @IsOptional()
    additional?: string;

    @IsString()
    @MaxLength(10)
    @MinLength(4)
    zip: string;

    @IsString()
    @MaxLength(200)
    @Matches(nameRegex)
    city: string;

    @IsString()
    @MaxLength(200)
    @IsOptional()
    @Matches(nameRegex)
    province?: string;

    @IsBoolean()
    @IsOptional()
    twoFactorAuth?: boolean;
}