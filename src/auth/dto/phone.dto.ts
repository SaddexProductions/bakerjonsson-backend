//npm modules
import { Matches, MinLength, IsString } from "class-validator";

//local modules
import { countryRegex } from "../miscConstants";

export class PhoneDTO {

    @IsString()
    @Matches(/[0-9]{0,14}$/)
    phone: string;
    
    @IsString()
    @MinLength(2)
    @Matches(countryRegex)
    phoneCountryCode: string;
}