//npm modules
import { Types } from "mongoose";

//local modules
import { ProductDocument } from "src/products/product.schema";

export interface ReturnSigninInterface {
    token: string;
    user: ReturnUserInterface;
    deviceToken?: string;
}

export interface AddressBeforeEncryption {
    street: string;
    zip: string;
    city: string;
    province?: string;
    additional?: string;
    countryCode?: string;
}

export interface ReturnUserInterface {

    _id: string;

    email: string;

    phonenumber: string;

    countryCode: string;

    phoneCountryCode: string;

    twoFactorAuth: boolean;

    firstName: string;

    lastName: string;

    address?: {
        street: string;
        city: string;
        province?: string;
        zip: string;
        additional?: string;
    }

    shoppingCart: CartItem[];
}

export interface TwoFactorEnabledReturn {
    twoFactor: boolean;
    twoFactorToken: string;
}

export interface EncryptedAddressInterface {
    content: string;
    iv: string;
}

export interface CartItem {
    count: number;
    _id: Types.ObjectId | ProductDocument;
    discountPriceAtOrder?: number;
}

export interface DeviceToken {

    user: {
        _id: string;
        email: string;
    };
    date: string;
}