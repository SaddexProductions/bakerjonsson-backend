//npm modules
import { IsString, IsEmail, MaxLength, IsOptional, MinLength, Matches, IsBoolean } from "class-validator";

//local modules
import { passwordRegex, nameRegex, countryRegex } from "../miscConstants";

export class EditUserDTO {

    @IsString()
    password: string;

    @IsString()
    @IsEmail()
    @IsOptional()
    @MaxLength(200)
    email?: string;

    @IsString()
    @IsOptional()
    @MinLength(8)
    @MaxLength(25)
    @Matches(passwordRegex, {message: 'Password too weak'})
    newPassword?: string;

    @IsString()
    @MinLength(2)
    @MaxLength(20)
    @IsOptional()
    @Matches(nameRegex)
    firstName?: string;

    @IsString()
    @MinLength(2)
    @MaxLength(20)
    @IsOptional()
    @Matches(nameRegex)
    lastName?: string;

    @IsString()
    @Matches(/[0-9]{0,14}$/)
    @IsOptional()
    phone?: string;

    @IsString()
    @MinLength(2)
    @IsOptional()
    @Matches(countryRegex)
    countryCode?: string;

    @IsString()
    @MinLength(2)
    @IsOptional()
    @Matches(countryRegex)
    phoneCountryCode?: string;

    @IsString()
    @MinLength(4)
    @IsOptional()
    @MaxLength(200)
    street?: string;

    @IsString()
    @IsOptional()
    @MaxLength(10)
    @MinLength(4)
    zip?: string;

    @IsString()
    @MaxLength(200)
    @IsOptional()
    @Matches(nameRegex)
    province?: string;

    @IsString()
    @MaxLength(200)
    @IsOptional()
    @Matches(nameRegex)
    city?: string;

    @IsString()
    @MaxLength(50)
    @IsOptional()
    additional?: string;

    @IsBoolean()
    @IsOptional()
    twoFactorAuth?: boolean;
}