//local modules
import { UserDocument } from "../user.schema";
import { TokenEnum } from "./token.enum";

export class ResolvedTokenDTO {

    user: UserDocument;
    type: TokenEnum;
}