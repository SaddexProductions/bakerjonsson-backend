//npm modules
import { IsBoolean, IsOptional, IsString, MinLength, MaxLength, IsJWT } from "class-validator";

export class TwoFactorDTO {

    @IsString()
    @IsJWT()
    token: string;

    @IsString()
    @MinLength(7)
    @MaxLength(7)
    key: string;

    @IsBoolean()
    @IsOptional()
    saveDevice: boolean;
}