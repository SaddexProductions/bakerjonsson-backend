//npm modules
import { IsString, IsJWT, IsBoolean, IsOptional } from "class-validator";

export class TokenOrRejectDTO {

    @IsString()
    @IsJWT({message: "Invalid token format"})
    token: string;

    @IsBoolean()
    @IsOptional()
    rejectChange: boolean;
}