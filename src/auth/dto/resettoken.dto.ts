//npm modules
import { IsBoolean, IsOptional, IsString, IsJWT } from "class-validator";

export class ResetTokenDTO {

    @IsString()
    @IsJWT()
    token: string;

    @IsBoolean()
    @IsOptional()
    rejectChange?: boolean;
}