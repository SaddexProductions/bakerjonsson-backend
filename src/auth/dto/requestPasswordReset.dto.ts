//npm modules
import { IsString, IsEmail, MaxLength } from "class-validator";

export class RequestPasswordResetDTO {

    @IsString()
    @IsEmail()
    @MaxLength(200)
    email: string;
}