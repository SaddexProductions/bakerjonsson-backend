//local modules
import { UserDocument } from "../user.schema";
import { TokenEnum } from "./token.enum";

export class JwtPayload {

    constructor(user: UserDocument, public type: TokenEnum){
        this._id = user._id;
        this.isAdmin = user.isAdmin;
        this.email = user.email;
    }

    _id: string;
    email: string;
    isAdmin: boolean;
}