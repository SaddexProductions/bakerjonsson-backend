export enum TokenEnum {
    LOGIN = "LOGIN",
    RESET = "RESET",
    TWOFACTOR = "TWOFACTOR",
    CHANGEEMAIL = "CHANGEEMAIL"
}