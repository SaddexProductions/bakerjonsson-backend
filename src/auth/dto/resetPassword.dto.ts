//npm modules
import { IsString, MinLength, MaxLength, Matches, IsJWT } from "class-validator";

export class ResetPasswordDTO {

    @IsString()
    @IsJWT()
    token: string;

    @IsString()
    @MinLength(8)
    @MaxLength(25)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'Password too weak'})
    password: string;
}