//npm modules
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from "@nestjs/common";
import * as config from 'config';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

//local modules
import { JwtPayload } from "./dto/jwt-payload.dto";
import { UserDocument } from "./user.schema";
import { ResolvedTokenDTO } from "./dto/resolvedToken.dto";
import { clearProductOrders } from "src/shared/clearProductOrders";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(
        @InjectModel('User') private userModel: Model<UserDocument>
    ){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET || config.get('baker_jwtPrivateKey')
        });
    }

    async validate (payload: JwtPayload): Promise<ResolvedTokenDTO> {
        const {_id, type} = payload;
        
        const user = await this.userModel.findOne({_id})
        .populate({path: "shoppingCart._id", model: "Product"})
        .populate({path: "messages", model: "Message"}).exec();

        user.shoppingCart = clearProductOrders(user.shoppingCart);

        if(!user) throw new UnauthorizedException();

        return {
            user,
            type
        };
        
    }
}