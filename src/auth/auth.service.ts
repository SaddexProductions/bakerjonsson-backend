//npm modules
import { 
    Injectable, 
    NotFoundException, 
    BadRequestException, 
    InternalServerErrorException, 
    ForbiddenException, 
    Logger
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import * as hb from 'handlebars';
import { MailerService } from '@nestjs-modules/mailer';
import { Client } from 'authy-client';
import * as config from 'config';

//local modules
import { UserDocument } from './user.schema';
import { LoginDTO } from './dto/login.dto';
import { JwtPayload } from './dto/jwt-payload.dto';
import { 
    ReturnSigninInterface, 
    TwoFactorEnabledReturn, 
    ReturnUserInterface, 
    AddressBeforeEncryption, 
    DeviceToken 
} from './dto/interfaces';
import { SignUpDTO } from './dto/signup.dto';
import { readFileAsync } from 'src/shared/readFileAsync';
import { encrypt, decrypt } from 'src/shared/cipherDecipher';
import { saltEmail } from '../shared/saltEmail';
import { ResetPasswordDTO } from './dto/resetPassword.dto';
import { TwoFactorDTO } from './dto/twofactor.dto';
import { removeZero } from 'src/shared/removeZero';
import { NewAuthyInterface } from './new-authy.interface';
import { EditUserDTO } from './dto/edit-user.dto';
import { TokenEnum } from './dto/token.enum';
import { TokenOrRejectDTO } from './dto/token-or-reject.dto';
import { clearProductOrders } from 'src/shared/clearProductOrders';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ResetTokenDTO } from './dto/resettoken.dto';
import { capitalize } from 'src/shared/capitalize';
import { PhoneDTO } from './dto/phone.dto';

//setup
const key = config.get('baker_authy_key');
const client = new Client({ key });

@Injectable()
export class AuthService {

    private readonly logger = new Logger("Authservice");

    //constructor, injection of dependencies
    constructor(
        @InjectModel('User') private userModel: Model<UserDocument>,
    private jwtService: JwtService, private mailerService: MailerService){}

    //host url for front-end
    static readonly host = process.env.NODE_ENV === "development" ? "http://localhost:4200" : 
    "https://bagarejoensson.saddexproductions.com";

    async checkEmail (email: string, user: UserDocument): Promise<SuccessfulOPDTO> {

        if (user && email === user.email) return {
            success: true
        };

        const emailUsed = await this.userModel.findOne({email: email.toLowerCase()});

        return {
            success: !emailUsed
        }
    }

    async checkPhone (phoneDTO: PhoneDTO, user: UserDocument): Promise<SuccessfulOPDTO> {

        const { phone, phoneCountryCode } = phoneDTO;

        if (user && user.phonenumber === phone && user.phoneCountryCode === phoneCountryCode) return {
            success: true
        };

        const phoneUsed = await this.userModel.findOne({phonenumber: removeZero(phone.replace(/ /g, "")), 
        phoneCountryCode: phoneCountryCode.toUpperCase()});

        return {
            success: !phoneUsed
        }
    }

    async login (loginDTO: LoginDTO): Promise<ReturnSigninInterface | TwoFactorEnabledReturn> {

        const {email, password, deviceToken} = loginDTO;
        const user = await this.userModel.findOne({email: email.toLowerCase()})
        .populate({path: "shoppingCart._id", model: "Product"});

        if(!user) throw new NotFoundException('A user with this email does not exist');

        const validPassword = await bcrypt.compare(password, user.password);
        if(!validPassword) throw new BadRequestException('Invalid email/password combination');

        let trustedDevice = false;

        if (deviceToken) {

            let decodedDeviceToken: DeviceToken = null;

            try {
                decodedDeviceToken = this.jwtService.verify(deviceToken);
            } catch (_) {
            }

            trustedDevice = decodedDeviceToken &&
            user._id.toString() === decodedDeviceToken.user._id;
        }

        if (user.twoFactorAuth && !trustedDevice) {

            const payload: JwtPayload = new JwtPayload(user, TokenEnum.TWOFACTOR);

            return {
                twoFactor: true,
                twoFactorToken: this.jwtService.sign(Object.assign({}, payload))
            }

        } else {
            const payload: JwtPayload = new JwtPayload(user, TokenEnum.LOGIN);
            const token = await this.jwtService.sign(Object.assign({}, payload));

            return {
                user: {
                    _id: user._id,
                    email: user.email,
                    shoppingCart: clearProductOrders(user.shoppingCart),
                    firstName: user.firstName,
                    lastName: user.lastName,
                    twoFactorAuth: user.twoFactorAuth,
                    phonenumber: user.phonenumber,
                    countryCode: user.countryCode,
                    phoneCountryCode: user.phoneCountryCode
                },
                token
            }
        }
    }

    async signup(signUpDTO: SignUpDTO): Promise<SuccessfulOPDTO> {

        const {
            email, 
            phone, 
            countryCode, 
            street,
            city,
            firstName,
            lastName,
            zip,
            additional,
            password,
            province,
            phoneCountryCode,
            twoFactorAuth
        } = signUpDTO;

        const cellphone = removeZero(phone.replace(/ /g, ""));

        const userExists = await this.userModel.find(
            { $or: 
            [ 
                { email: email.toLowerCase() }, 
                { phonenumber: cellphone, phoneCountryCode: phoneCountryCode ? phoneCountryCode.toUpperCase() : countryCode.toUpperCase() } 
            ] }
        );

        if (userExists.length > 0) throw new BadRequestException("An user with these credentials has already been registered");

        let authyID = null;

        if (twoFactorAuth) {

            if (!phone || !countryCode) throw new BadRequestException('Telephone or country ISO missing');

            try {

                authyID = await this.addAuthy({
                    countryCode: phoneCountryCode ? phoneCountryCode : countryCode,
                    email,
                    phone: cellphone
                });
            }
            catch (ex) {
                this.logger.error("2FA failed to setup. Reason: " + ex);
                throw new InternalServerErrorException("Two-factor Authentication failed to setup");
            }
        }

        const hashedPassword = await bcrypt.hash(password, 12);

        const addressFormat: AddressBeforeEncryption = {
            street: capitalize(street).trim(),
            zip: zip.replace(/ /g, "").toUpperCase().trim(),
            city: capitalize(city).trim(),
            province: capitalize(province).trim(),
            additional: capitalize(additional).trim()
        };

        const encryptedAddress = encrypt(JSON.stringify(addressFormat));

        const user = new this.userModel({
            email: email.toLowerCase(),
            password: hashedPassword,
            phonenumber: cellphone,
            firstName: capitalize(firstName).trim(),
            lastName: capitalize(lastName).trim(),
            countryCode: countryCode.toUpperCase(),
            phoneCountryCode: phoneCountryCode ? phoneCountryCode.toUpperCase() : countryCode.toUpperCase(),
            authyID,
            address: encryptedAddress,
            twoFactorAuth: !!twoFactorAuth,
            shoppingCart: [],
            conversations: []
        });
        
        await this.saveUser(user);

        //email should be sent whenever user is signed up

        return {
            success: true
        }
    }

    async requestPasswordReset (email: string): Promise<string> {

        const user = await this.userModel.findOne({email: email.toLowerCase()});
        if(!user) throw new NotFoundException("An user with this email was not found");

        const payload: JwtPayload = new JwtPayload(user, TokenEnum.RESET);
        
        const token = await this.jwtService.sign(Object.assign({}, payload));

        user.validResetToken = token;

        await this.saveUser(user);

        const url = `${AuthService.host}/auth/resetpassword?token=${token}`;

        try {
            await this.sendEmail(user, {
                url,
                clickHere: `${url}&reject=true`,
                subject: "Reset Password",
                buttonTitle: "Reset",
                content: `You or someone else have requested a password 
                reset for your account on bagarejoensson.saddexproductions.com. 
                Please click the link below to finish the
                process.`
            });
            return `A password reset link was sent to the email ${saltEmail(user.email)}!`;
        } catch (ex) {

            this.logger.error(`Failed to send email. Reason: ${ex}`, ex);

            throw ex;
        }
    }

    async validateResetToken (validateResetTokenDTO: ResetTokenDTO): Promise<SuccessfulOPDTO> {

        const { rejectChange, token } = validateResetTokenDTO;

        if (!token) throw new BadRequestException("Token is missing");

        const decoded: JwtPayload = this.decodeToken(token);

        const user = await this.userModel.findById(decoded._id);
        if(!user) throw new NotFoundException("User does not exist");

        if (rejectChange) {
            user.validResetToken = null;
            await this.saveUser(user);

            return {
                success: false,
                rejectedChange: true
            }
        }

        return {
            success: token === user.validResetToken && decoded.type === TokenEnum.RESET
        }
    }

    async resetPassword (resetPasswordDTO: ResetPasswordDTO): Promise<SuccessfulOPDTO> {

        const { password, token } = resetPasswordDTO;

        const decoded: JwtPayload = this.decodeToken(token);
        if (decoded.type !== TokenEnum.RESET) throw new ForbiddenException("This token can't be used for resetting");

        const user = await this.userModel.findById(decoded._id);
        if (!user) throw new NotFoundException("User not found");

        if (user.validResetToken !== token) throw new BadRequestException("This token has already been spent");

        const isEqual = await bcrypt.compare(password, user.password);
        if (isEqual) throw new BadRequestException("The new password can't be same as the old password");

        user.password = await bcrypt.hash(password, 12);
        user.validResetToken = null;

        await this.saveUser(user);

        return {
            success: true
        }
    }

    async twoFactorAuth (twoFactorDTO: TwoFactorDTO): Promise<ReturnSigninInterface> {

        const { key, saveDevice, token } = twoFactorDTO;

        const decoded = this.decodeToken(token);
        if (decoded.type !== TokenEnum.TWOFACTOR) throw new ForbiddenException("This token can't be use for 2FA");

        const user = await this.userModel.findById(decoded._id)
        .populate({path: "shoppingCart._id", model: "Product"});
        
        if (!user) throw new NotFoundException("User was not found");

        try {
            const { success } = await client.verifyToken({ authyId: user.authyID, token: key });

            if (success) {

                const payload: JwtPayload = new JwtPayload(user, TokenEnum.LOGIN);

                const token = this.jwtService.sign(Object.assign({}, payload));

                let deviceToken = null;

                if (saveDevice) {
                    const deviceTokenPayload: DeviceToken = {
                        user: {
                            email: user.email,
                            _id: user._id
                        },
                        date: new Date().toISOString()
                    }

                    deviceToken = this.jwtService.sign(deviceTokenPayload);
                }

                return {
                    token,
                    user: {
                        email: user.email,
                        _id: user._id,
                        shoppingCart: clearProductOrders(user.shoppingCart),
                        phonenumber: user.phonenumber,
                        countryCode: user.countryCode,
                        firstName: user.firstName,
                        twoFactorAuth: user.twoFactorAuth,
                        lastName: user.lastName,
                        phoneCountryCode: user.phoneCountryCode
                    },
                    deviceToken
                };
            }
        }
        catch (_) {
            throw new BadRequestException("Invalid two-factor token");
        }
    }

    async sendSms (token: string): Promise<SuccessfulOPDTO> {

        if(!token) throw new BadRequestException("Token missing");

        const decoded = this.decodeToken(token);
        if (decoded.type !== TokenEnum.TWOFACTOR) throw new ForbiddenException("This token can't be use for 2FA");

        const user = await this.userModel.findById(decoded._id);
        if (!user) throw new NotFoundException("User was not found");

        await client.requestSms({ authyId: user.authyID });

        return {
            success: true
        }
    }

    async editUser (editUserDTO: EditUserDTO, user: UserDocument): Promise<ReturnUserInterface> {

        const {
            password,
            email,
            newPassword,
            countryCode,
            phone,
            firstName,
            lastName,
            phoneCountryCode,
            additional,
            street,
            city,
            zip,
            province,
            twoFactorAuth
        } = editUserDTO;

        if (email) {
            const emailExists = await this.userModel.findOne({email: email.toLowerCase()});
            if (emailExists) throw new BadRequestException("Another user already has that email registered");
        }

        const correctPassword = await bcrypt.compare(password, user.password);
        if (!correctPassword) throw new BadRequestException("Invalid password");

        if (newPassword) {

            const isEqual = await bcrypt.compare(newPassword, user.password);
            if (isEqual) throw new BadRequestException("The new password can't be the same as the old password");

            const hashedNewPassword = await bcrypt.hash(newPassword, 12);
            user.password = hashedNewPassword;
        }

        if (firstName) {
            user.firstName = capitalize(firstName).trim();
        }

        if (lastName) {
            user.lastName = capitalize(lastName).trim();
        }

        if (additional || city || zip || street || province || countryCode) {

            if (countryCode) {
                user.countryCode = countryCode.toUpperCase();
            }

            const decryptedAddress: AddressBeforeEncryption = JSON.parse(decrypt(user.address));

            const objToCycle = {
                additional: additional ? capitalize(additional).trim() : null,
                province: province ? capitalize(province).trim() : null,
                street: street ? capitalize(street).trim() : null,
                zip: zip ? zip.toUpperCase().trim() : null,
                city: city ? capitalize(city).trim() : null,
                countryCode: countryCode ? countryCode.toUpperCase() : null
            }

            for (const key in objToCycle) {

                if (objToCycle[key]) decryptedAddress[key] = objToCycle[key];
            }

            user.address = encrypt(JSON.stringify(decryptedAddress));
        }

        if ((user.twoFactorAuth && phone) || (user.twoFactorAuth && phoneCountryCode) ||
        (!user.twoFactorAuth && twoFactorAuth)) {

            if (user.twoFactorAuth && twoFactorAuth) await client.deleteUser({ authyId: user.authyID });

            const cellphone = phone ? removeZero(phone.replace(/ /g, "")) : user.phonenumber;

            const authyID = await this.addAuthy({
                phone: cellphone,
                email: email ? email.toLowerCase() : user.email,
                countryCode: phoneCountryCode
            });

            user.authyID = authyID;
            user.phonenumber = cellphone;
            user.twoFactorAuth = true;

        }   else if (twoFactorAuth === false && user.twoFactorAuth) {
            await client.deleteUser({ authyId: user.authyID });
            user.twoFactorAuth = false;
            user.authyID = null;
        }

        if (email) {

            const payload: JwtPayload = new JwtPayload(user, TokenEnum.CHANGEEMAIL);
            const token = this.jwtService.sign(Object.assign({}, payload));
            user.newEmail = {
                email: email.toLowerCase(),
                newEmailToken: token
            };

            const url = `${AuthService.host}/auth/confirmemailupdate?token=${token}`

            try {
                await this.sendEmail(user, {

                    subject: "Email address change",
                    content: `A request has been made to change the associated email address on 
                    bagarejoensson.saddexproductions.com from ${user.email} to ${email}. 
                    Confirm the change by clicking the button below.`,
                    buttonTitle: "Confirm Email",
                    url,
                    clickHere: `${url}&reject=true`
                });
            }
            catch (ex) {

                this.logger.error(`Failed to send email. Reason: ${ex}`, ex);

                throw ex;
            }
        }

        const result = await this.saveUser(user);

        return {
            _id: result._id,
            email: result.email,
            phonenumber: result.phonenumber,
            twoFactorAuth: result.twoFactorAuth,
            firstName: result.firstName,
            lastName: result.lastName,
            countryCode: result.countryCode,
            phoneCountryCode: result.phoneCountryCode,
            shoppingCart: result.shoppingCart,
            address: JSON.parse(decrypt(result.address))
        }
    }

    async confirmOrRejectNewEmail (tokenRejectDTO: TokenOrRejectDTO): Promise<SuccessfulOPDTO> {

        const { token, rejectChange } = tokenRejectDTO;
        const decoded = this.decodeToken(token);

        if (decoded.type !== TokenEnum.CHANGEEMAIL) 
            throw new ForbiddenException("This token can't be used for changing email address");

        const user = await this.userModel.findById(decoded._id);
        if (!user) throw new NotFoundException("User not found");

        if (!user.newEmail || user.newEmail && user.newEmail.newEmailToken !== token) 
            throw new BadRequestException("This token is no longer valid");

        if (rejectChange) {

            user.newEmail = null;
            await this.saveUser(user);

            return {
                success: false
            }
        }

        user.email = user.newEmail.email;
        user.newEmail = null;
        await this.saveUser(user);

        return {
            success: true
        }
    }

    async getUserWithAddress (user: UserDocument): Promise<ReturnUserInterface> {

        return {
            _id: user._id,
            email: user.email,
            phonenumber: user.phonenumber,
            countryCode: user.countryCode,
            phoneCountryCode: user.phoneCountryCode,
            twoFactorAuth: user.twoFactorAuth,
            address: JSON.parse(decrypt(user.address)),
            firstName: user.firstName,
            lastName: user.lastName,
            shoppingCart: user.shoppingCart
        }
    }

    private async saveUser (user: UserDocument) {
        
        try {
            return user.save();
        }  catch (ex) {
            this.logger.error("Save user failed. Reason: " + ex, ex);
        }
    }

    private decodeToken (token: string): JwtPayload {
        try {
            return this.jwtService.verify(token);
        }   
        catch (_) {
            throw new BadRequestException("Invalid token");
        }
    }

    private async addAuthy (userData: NewAuthyInterface): Promise<string> {
        const { user: { id: authyId } } = await client.registerUser(userData);
        return authyId;
    }

    private async sendEmail (user: UserDocument, 
        replacements: {
            url: string; 
            content: 
            string; 
            subject: string; 
            buttonTitle: string;
            clickHere: string;
        }): Promise<any> {

        const { url, content, subject, buttonTitle, clickHere } = replacements;

        const html = await readFileAsync(`${process.env.NODE_ENV === "development" ? 'src' :
        '..'}/templates/auth-template.html`, { encoding: 'utf-8' });
        let template = hb.compile(html);

        const templateToSend = template({
            title: subject,
            username: user.firstName,
            content,
            url,
            buttonTitle,
            clickHere 
        });

        return this.mailerService.sendMail({
            to: user.email, // list of receivers
            subject, // Subject line
            html: templateToSend
        });
    }
}
