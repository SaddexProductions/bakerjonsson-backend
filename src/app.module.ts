//npm modules
import { Module } from '@nestjs/common';
import * as config from 'config';
import { MongooseModule } from '@nestjs/mongoose';
import { MailerModule } from '@nestjs-modules/mailer';
import { ScheduleModule } from '@nestjs/schedule';

//local modules
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';
import { ReviewsModule } from './reviews/reviews.module';
import { OrdersModule } from './orders/orders.module';
import { CountriesModule } from './countries/countries.module';
import { ContactModule } from './contact/contact.module';

@Module({
  imports: [
    AuthModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://localhost/Bakerjoensson'),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.sendgrid.net',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: "apikey",
            pass: config.get('nodemailer_key')
        }
      },
      defaults: {
        from:'noreply@saddexproductions.com',
      }
    }),
    ProductsModule,
    ReviewsModule,
    OrdersModule,
    CountriesModule,
    ContactModule
  ]
})
export class AppModule {}
