//npm modules
import { 
    Controller, 
    Get, 
    Query, 
    ParseIntPipe, 
    Param, 
    Post, 
    UseGuards, 
    Body, 
    Put, 
    Delete, 
    ValidationPipe,
    UsePipes} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

//local modules
import { UserDocument, User } from 'src/auth/user.schema';
import { GetUser } from 'src/auth/get-user.decorator';
import { ProductsService } from './products.service';
import { GetProductsDTO } from './dto/get-products.dto';
import { ProductDocument } from './product.schema';
import { AddProductDTO } from './dto/add-product.dto';
import { CartItem } from 'src/auth/dto/interfaces';
import { ObjectIdDTO } from '../shared/objectid.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { OptionalJwtAuthGuard } from 'src/auth/optional-auth.guard';
import { GetEqualProductsDTO } from './dto/get-equal-products.dto';
import { CheckIfOrderedReviewed } from './dto/check-if-ordered.interface';
import { ProductReturn } from './dto/product-return.interface';

@Controller('products')
export class ProductsController {

    constructor (private productsService: ProductsService){}

    @Get('/')
    @UseGuards(OptionalJwtAuthGuard)
    @UsePipes(new ValidationPipe({transform: true}))
    getAllProducts (@Query() getProductsDTO: GetProductsDTO,
    @GetUser({ignoreError: true}) user: UserDocument): Promise<ProductReturn[]> {
        return this.productsService.getAllProducts(getProductsDTO, user ? user._id : null);
    }

    @Get('/getequalamount')
    @UseGuards(OptionalJwtAuthGuard)
    getEqualAmounts(@Query(ValidationPipe) getEqualProductsDTO: GetEqualProductsDTO,
    @GetUser({ignoreError: true}) user: UserDocument): Promise<ProductReturn[]> {
        
        const { limit } = getEqualProductsDTO;
        return this.productsService.returnEqualAmount(limit, user ? user._id : null);
    }

    @Get('/:id')
    @UseGuards(OptionalJwtAuthGuard)
    getProduct (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @GetUser({ignoreError: true}) user: UserDocument): Promise<ProductReturn> {
        const {id} = objectIdDTO;

        return this.productsService.getProduct(id, user ? user._id : null);
    }

    @Get('/:id/checkorderreview')
    @UseGuards(AuthGuard())
    checkIfOrderedAndReviewed (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @GetUser() user: UserDocument): Promise<CheckIfOrderedReviewed> {

        const { id } = objectIdDTO;

        return this.productsService.checkIfOrderedReviewed(id, user);
    }

    @Post('/cart/:id')
    @UseGuards(AuthGuard())
    addToBasket (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument, 
    @Body('count', ParseIntPipe) count: number): Promise<CartItem> {
        const {id} = objectIdDTO;
        return this.productsService.addToCart(id, count, user);
    }

    @Put('/cart/:id')
    @UseGuards(AuthGuard())
    changeCartItem (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument, 
    @Body('count', ParseIntPipe) count: number): Promise<CartItem> {

        const {id} = objectIdDTO;

        return this.productsService.updateCartItem(id, count, user);
    }

    @Delete('/cart/clear')
    @UseGuards(AuthGuard())
    clearCart (@GetUser() user: UserDocument): Promise<SuccessfulOPDTO> {
        return this.productsService.clearCart(user);
    }

    @Delete('/cart/:id')
    @UseGuards(AuthGuard())
    deleteCartItem (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument): Promise<SuccessfulOPDTO> {
        const {id} = objectIdDTO;

        return this.productsService.deleteCartItem(id, user);
    }

    @Post('/')
    @UseGuards(AuthGuard())
    addProduct (@Body(ValidationPipe) addProductDTO: AddProductDTO, 
    @GetUser({adminRequired: true}) _: User): Promise<ProductDocument> {

        return this.productsService.addProduct(addProductDTO);
    }
}
