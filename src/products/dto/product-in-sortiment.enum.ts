export enum InSortiment {
    COMING_SOON = "COMING_SOON",
    AVAILABLE = "AVAILABLE",
    EXPIRED = "EXPIRED"
};