//local modules
import { Review, ReviewDocument } from "src/reviews/review.schema";
import { InSortiment } from "./product-in-sortiment.enum";
import { ProductType } from "./product.enum";

export interface ProductReturn {

    _id?: string;
    title: string;
    averageRating: number;
    imageUrl: string;
    description: string;
    type: ProductType;
    tags: string[];
    price: number;
    stock: number;
    inSortiment: InSortiment;
    reviews?: string[] | ReviewDocument[];
    hasOrdered?: boolean;
    hasReviewed?: Review;
    discountPrice?: number;
}