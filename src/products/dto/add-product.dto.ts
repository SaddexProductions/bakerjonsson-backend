//npm modules
import { IsString, 
    MinLength, 
    MaxLength, 
    Matches, 
    IsInt, 
    IsNumber, 
    Min, 
    Max, 
    IsOptional, 
    IsEnum} from "class-validator";

//local modules
import { nameRegex } from "src/auth/miscConstants";
import { ProductType } from "./product.enum";

export class AddProductDTO {

    @IsString()
    @MinLength(2)
    @MaxLength(150)
    @Matches(nameRegex)
    title: string;

    @IsString()
    @MinLength(5)
    @MaxLength(350)
    description: string;

    @IsNumber()
    @IsInt()
    @Min(0)
    @Max(2000)
    @IsOptional()
    stock?: number;

    @IsString({each: true})
    @MinLength(3, {each: true})
    @MaxLength(30, {each: true})
    tags: string[];

    @IsString()
    @IsEnum(ProductType, {message: "Value must be uppercase and either 'BREAD', 'SWEET' or 'COOKIE"})
    type: ProductType;

    @IsNumber()
    @Min(0)
    @Max(30)
    price: number;

    @IsString()
    @MaxLength(500)
    imageUrl: string;
}