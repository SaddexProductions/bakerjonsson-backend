//local modules
import { ReviewDocument } from "src/reviews/review.schema";

export interface CheckIfOrderedReviewed {
    hasOrdered: boolean;
    hasReviewed: ReviewDocument;
}