//npm modules
import { IsNumber, IsInt } from "class-validator";
import { Transform } from "class-transformer";

export class GetEqualProductsDTO {

    @IsInt()
    @IsNumber()
    @Transform(value => Number(value))
    limit: number;
}