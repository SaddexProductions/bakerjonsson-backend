//npm modules
import { 
    IsOptional, 
    Min, 
    IsInt, 
    IsNumber, 
    IsString, 
    IsEnum, 
    MaxLength, 
    Max, 
    Matches,
    IsArray,
    IsBoolean,
    } from "class-validator";
import { Transform } from 'class-transformer';

//local modules
import { ProductType } from "./product.enum";
import { objectIdRegex } from "src/shared/objectid-regex";

export class GetProductsDTO {

    @IsNumber()
    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(1)
    offset?: number;

    @IsNumber()
    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(0)
    limit?: number;

    @IsString({each: true})
    @IsOptional()
    @IsArray()
    @IsEnum(ProductType, {message: "Value must be uppercase and either 'BREAD', 'SWEET' or 'COOKIE", each: true})
    type?: ProductType[];

    @IsArray()
    @IsString({each: true})
    @IsOptional()
    @MaxLength(150, {each: true})
    search?: string[];

    @IsString()
    @Matches(/true|false/)
    @IsOptional()
    available?: string;

    @IsNumber()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(1)
    @Max(5)
    lowerrating?: number;

    @IsNumber()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(1)
    @Max(5)
    higherrating?: number;

    @IsNumber()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(0)
    @Max(20)
    lowerprice?: number;

    @IsNumber()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(0)
    @Max(20)
    higherprice?: number;

    @IsOptional()
    @IsArray()
    @IsString({each: true})
    @Matches(objectIdRegex, {each: true, message: "Not a valid objectId"})
    product_ids?: string[];
}