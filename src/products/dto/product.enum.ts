export enum ProductType {

    SWEET = "SWEET",
    BREAD = "BREAD",
    COOKIE = "COOKIE"
}