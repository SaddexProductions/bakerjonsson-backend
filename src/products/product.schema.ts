//npm modules
import { Document, SchemaTypes } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

//local modules
import { ProductType } from './dto/product.enum';
import { OrderDocument } from 'src/orders/order.schema';
import { InSortiment } from './dto/product-in-sortiment.enum';
import { ReviewDocument } from 'src/reviews/review.schema';

const {ObjectId} = SchemaTypes;

export type ProductDocument = Product & Document;

@Schema()
export class Product {

    @Prop({
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
    })
    title: string;

    @Prop({
        type: String,
        enum: Object.keys(ProductType)
        .map(key => ProductType[key]),
        required: true
    })
    type: ProductType;

    @Prop({
        type: Number,
        required: true,
        min: 0,
        max: 20
    })
    price: number;

    @Prop({
        type: Number,
        min: 0,
        max: 20
    })
    discountPrice?: number;

    @Prop({
        type: String,
        required: true,
        minlength: 10,
        maxlength: 350
    })
    description: string;

    @Prop({type: [String], required: true})
    tags: string[];

    @Prop({
        type: String,
        required: true,
        maxlength: 500
    })
    imageUrl: string;

    @Prop({
        required: true,
        type: String,
        enum: Object.keys(InSortiment)
        .map(key => InSortiment[key]),
        default: InSortiment.AVAILABLE
    })
    inSortiment: InSortiment;

    @Prop({
        type: Number,
        required: true,
        min: 0,
        default: 20
    })
    stock: number;

    @Prop({
        type: [ObjectId],
        ref: "Order",
        default: []
    })
    orders: string[] | OrderDocument[]; //not returned to the user

    @Prop({
        type: [ObjectId],
        ref: "Review",
        default: []
    })
    reviews: string[] | ReviewDocument[];

    @Prop({
        type: Number,
        min: 1,
        max: 5,
        default: null
    })
    averageRating: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);