//npm modules
import { Injectable, NotFoundException, BadRequestException, Logger, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

//local modules
import { GetProductsDTO } from './dto/get-products.dto';
import { ProductDocument, Product } from './product.schema';
import { AddProductDTO } from './dto/add-product.dto';
import { UserDocument } from 'src/auth/user.schema';
import { CartItem } from 'src/auth/dto/interfaces';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { OrderDocument } from 'src/orders/order.schema';
import { OrderStatus } from 'src/orders/dto/order-status.enum';
import { ProductType } from './dto/product.enum';
import { ReviewDocument, Review } from 'src/reviews/review.schema';
import { CheckIfOrderedReviewed } from './dto/check-if-ordered.interface';
import { ProductReturn } from './dto/product-return.interface';

@Injectable()
export class ProductsService {

    private readonly logger = new Logger("Productservice");

    constructor(@InjectModel('Product') private productModel: Model<ProductDocument>,
    @InjectModel('Order') private orderModel: Model<OrderDocument>,
    @InjectModel('Review') private reviewModel: Model<ReviewDocument>,
    @InjectModel('User') private userModel: Model<UserDocument>){}

    async getAllProducts (getProductsDTO: GetProductsDTO, userid: string): Promise<ProductReturn[]> {

        const aggregatePipeline: Object[] = [];

        const { 
            type, 
            lowerrating, 
            higherrating, 
            search, 
            available, 
            offset, 
            limit,
            lowerprice,
            product_ids,
            higherprice
        } = getProductsDTO;

        if (product_ids) {

            aggregatePipeline.push({$match: {
                _id: {$in: product_ids.map(id => Types.ObjectId(id))}
            }});
        }

        if (type) {
            aggregatePipeline.push({$match: {type: {$in: type}}});
        }

        if (available === "true") {
            aggregatePipeline.push({$match: {stock: {$gt: 0}}});
        }

        if (lowerrating && higherrating) {

            if (lowerrating > higherrating) 
                throw new BadRequestException("Lower rating limit can't be higher than the upper one");

            aggregatePipeline.push({$match: {$or: [{averageRating: {$gte: lowerrating, $lte: higherrating}},
                {averageRating: null}]}});
        }

        if (lowerprice !== undefined && higherprice !== undefined) {

            if (lowerprice > higherprice) 
                throw new BadRequestException("Lower price limit can't be higher than the upper one");

            aggregatePipeline.push({$match: {price: {$gte: lowerprice, $lte: higherprice}}});
        }

        if (search) {

            const searchString = new RegExp(search.join('|'), 'i');

            aggregatePipeline.push({$match: {
            $or: [{
                title: searchString
            }, 
            {
                tags: searchString
            },
            {
                description: searchString
            }]
            }});

            aggregatePipeline.push({$set: {
                relevance: {
                    $sum: [
                        {$multiply: [{$size: {$regexFindAll: {input: "$title", regex: searchString}}}, 100]},
                        {$multiply: [{$size: {$regexFindAll: {input: {
                            $reduce: {
                               input: "$tags",
                               initialValue: "",
                               in: { $concat : ["$$value", " ", "$$this"] }
                            }
                        }, regex: searchString}}}, 50]},
                        {$multiply: [{$size: {$regexFindAll: {input: "$description", regex: searchString}}}, 30]},
                    ]
                }
            }});

            aggregatePipeline.push({$match: {relevance: {$gte: search.length * 25}}})

            aggregatePipeline.push({$sort: {relevance: -1}});
        }  else {
            aggregatePipeline.push({$sort: {title: 1}});
        }

        const skip = +offset || 0;
        const results = +limit || 10;

        aggregatePipeline.push( 
        { "$lookup": {
          "from": "reviews",
          "foreignField": "_id",
          "localField": "reviews",
          "as": "reviews"
        }});

        aggregatePipeline.push({$skip: skip});
        aggregatePipeline.push({$limit: results});
        
        const products = await this.productModel.aggregate(aggregatePipeline);

        let orders: OrderDocument[];
        let orderIds: string[];
        let reviews: ReviewDocument[];

        if (userid) {
            orders = await this.orderModel.find({"user._id": Types.ObjectId(userid), status: OrderStatus.DELIEVERED});
            orderIds = orders.map((order: OrderDocument) => order._id.toHexString());

            reviews = await this.reviewModel.find({"creator._id": Types.ObjectId(userid)});
        }

        const productsToReturn = await Promise.all<ProductReturn>(products.map(async (product: ProductDocument): Promise<ProductReturn> => {

            let hasOrdered;
            let hasReviewed;

            await this.processEmbeddedReviews(product, <ReviewDocument[]>product.reviews, userid);

            if (orderIds) {

                hasOrdered = this.checkIfOrdered(product, orderIds);
            }

            if (reviews && reviews.length > 0) {

                const myReview = reviews.find(r => r.product._id.toString() === product._id.toString());
                if (myReview) hasReviewed = myReview;
            }

            return this.convertDocToReturnType(product, hasOrdered, hasReviewed);
        }));

        return productsToReturn;
    }

    async returnEqualAmount (limit: number, userid: string): Promise<ProductReturn[]> {

        if (limit % 3 !== 0) throw new BadRequestException("Limit must be dividable by 3");

        const results = await Promise.all(
            [
            this.getAllProducts({limit: limit / 3, type: [ProductType.BREAD], available: "true" }, userid),
            this.getAllProducts({limit: limit / 3, type: [ProductType.SWEET], available: "true" }, userid),
            this.getAllProducts({limit: limit / 3, type: [ProductType.COOKIE], available: "true" }, userid)
            ]
        );

        return [].concat.apply([], results);
    }

    async checkIfOrderedReviewed (id: string, user: UserDocument): Promise<CheckIfOrderedReviewed> {

        const product = await this.productModel.findById(id);
        if (!product) throw new NotFoundException("Product not found");

        const orders = await this.orderModel.find({"user._id": user._id, status: OrderStatus.DELIEVERED});
        const orderIds = orders.map((order: OrderDocument) => order._id.toHexString());
        
        const hasOrdered = this.checkIfOrdered(product, orderIds);

        const myReview = await this.reviewModel.findOne({"creator._id": user._id, "product._id": product._id});
        
        if (myReview) {

            myReview.votes = null;
            myReview.creator.name = `${user.firstName} ${user.lastName.charAt(0)}`;
        }

        return {
            hasReviewed: myReview,
            hasOrdered
        };
    }

    async getProduct(id: string, userid: string): Promise<ProductReturn> {

        const product = await this.productModel.findById(id).populate({path: 'reviews',
        model: "Review",
        options: {
            limit: 20,
            sort: {voteSum: -1}
        }});
        if (!product) throw new NotFoundException();

        let hasOrdered: boolean;
        let hasReviewed: Review;

        if (userid) {

            const orders = await this.orderModel.find({"user._id": Types.ObjectId(userid), status: OrderStatus.DELIEVERED});
            const orderIds = orders.map((order: OrderDocument) => order._id.toHexString());
            
            hasOrdered = this.checkIfOrdered(product, orderIds);
            hasReviewed = await this.reviewModel.findOne({"product._id": product._id, "creator._id": Types.ObjectId(userid)});

            if (hasReviewed) hasReviewed.votes = null;
        }

        const reviews = <ReviewDocument[]>product.reviews;

        await this.processEmbeddedReviews(product, reviews, userid);

        return this.convertDocToReturnType(product, hasOrdered, hasReviewed);
    }

    async addProduct (addProductDTO: AddProductDTO): Promise<ProductDocument> {

        const { title, description, tags, price, stock, type, imageUrl } = addProductDTO;

        const productExists = await this.productModel.findOne({title});
        if (productExists) throw new BadRequestException("This product already exists");

        const product = new this.productModel({
            title,
            description,
            tags: tags.map(t => t.toLowerCase()),
            price,
            type,
            imageUrl
        });

        if (stock || stock === 0) product.stock = stock;

        return product.save();
    }

    async addToCart (id: string, count: number, user: UserDocument): Promise<CartItem> {

        if (count <= 0) throw new BadRequestException("Count must be higher than 0");

        const product = await this.productModel.findById(id);
        if (!product) throw new NotFoundException("Product not found");

        if (count > product.stock) throw new BadRequestException("Order size is larger than currently available product stock");

        const newCartItem: CartItem = {
            count,
            _id: Types.ObjectId(id)
        }

        if (user.shoppingCart.length > 0) {

            const itemExists = this.filterShoppingCart(this.convertCart(user.shoppingCart), id).length > 0;
            if (itemExists) throw new BadRequestException("This item has already been added to the cart");
        }

        user.shoppingCart.push(newCartItem);

        await this.saveUser(user);

        return {
            _id: product,
            count
        }
    }

    async updateCartItem (id: string, countToAdd: number, user: UserDocument): Promise<CartItem> {

        const product = await this.productModel.findById(id);
        if (!product) throw new NotFoundException("Product not found");

        const convertedCart = this.convertCart(user.shoppingCart);

        const itemToChange = this.filterShoppingCart(convertedCart, id);
        if (itemToChange.length === 0) 
            throw new NotFoundException("An item with this id is not in the shopping cart of this user");
        const indexOfItemToChange = this.getIndexOfCartItem(convertedCart, id);

        if (itemToChange[0].count + countToAdd <= 0) 
            throw new BadRequestException("The new amount of items must be more than 0");
        if (itemToChange[0].count + countToAdd > product.stock) 
            throw new BadRequestException("The new amount of items exceeds available stock");

        itemToChange[0].count += countToAdd;

        user.shoppingCart[indexOfItemToChange] = itemToChange[0];

        await this.saveUser(user);

        return itemToChange[0];
    }

    async deleteCartItem (id: string, user: UserDocument): Promise<SuccessfulOPDTO> {

        const product = await this.productModel.findById(id);
        if (!product) throw new NotFoundException("Product not found");

        const indexOfItemToDelete = this.getIndexOfCartItem(this.convertCart(user.shoppingCart), id);
        if (indexOfItemToDelete === -1) throw new NotFoundException("An item with this id is not in the shopping cart of this user");

        user.shoppingCart.splice(indexOfItemToDelete, 1);
        await user.save();
        
        return {
            success: true
        }
    }

    async clearCart (user: UserDocument): Promise<SuccessfulOPDTO> {

        user.shoppingCart = [];
        await this.saveUser(user);

        return {
            success: true
        }
    }

    private async processEmbeddedReviews (product: ProductDocument, 
        reviews: ReviewDocument[],
        userid?: string): Promise<void[]> {

        return Promise.all(reviews.map(async (review, index: number) => {

            const castedReview = <ReviewDocument>review;

            const user = await this.userModel.findById(castedReview.creator._id);

            castedReview.creator.name = `${user.firstName} ${user.lastName.charAt(0)}`;

            if (userid) {
                
                const hasVoted = review.votes.filter(v => v._id.toString() === userid.toString());
                
                if (hasVoted && hasVoted.length > 0) review.hasVoted = {
                    positive: hasVoted[0].positive
                };
            }

            castedReview.votes = null;
            
            product.reviews[index] = castedReview;
        }));
    }

    private convertDocToReturnType 
    (product: ProductDocument, hasOrdered?: boolean, hasReviewed?: Review): ProductReturn {
        
        const toReturn: ProductReturn = {
            _id: product._id,
            title: product.title,
            description: product.description,
            type: product.type,
            price: product.price,
            stock: product.stock,
            reviews: product.reviews,
            imageUrl: product.imageUrl,
            averageRating: product.averageRating,
            tags: [...product.tags],
            inSortiment: product.inSortiment
        }

        if (hasOrdered) toReturn.hasOrdered = hasOrdered;
        if (hasReviewed) toReturn.hasReviewed = hasReviewed;
        if (product.discountPrice) toReturn.discountPrice = product.discountPrice;

        return toReturn;
    }

    private checkIfOrdered (product: ProductDocument, orderIds: string[]): boolean {
        const productOrders = <string[]>[...product.orders].map(order => order.toString());

        return productOrders.filter(order => orderIds.indexOf(order) > -1).length > 0;
    }

    private async saveUser (user: UserDocument): Promise<UserDocument> {

        try {
            return user.save();

        }  catch (ex) {
            this.logger.error(`Saving user failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }

    private convertCart (cart: CartItem[]) {
        return cart.map(item => {

            const castedItem = <ProductDocument>item._id;

            return {
                count: item.count,
                _id: castedItem._id
            }

        });
    }

    private getIndexOfCartItem (array: CartItem[], id: string): number {
        return array.findIndex((item) => {
            if (item._id instanceof Product) {
                return false;
            } else {
                return (item._id.toHexString() === id)
            }
        });
    }

    private filterShoppingCart (array: CartItem[], id: string): CartItem[] {

        return array.filter((item) => {
            if (item._id instanceof Product) {
                return false;
            } else {
                return (item._id.toHexString() === id)
            }
        });
    }
}
