//npm modules
import { Injectable, Logger, InternalServerErrorException, NotFoundException, ForbiddenException, BadRequestException } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as paypal from 'paypal-rest-sdk';
import * as config from 'config';
import { v4 as uuid } from 'uuid';
import { MailerService } from '@nestjs-modules/mailer';
import { Cron } from '@nestjs/schedule';

//local modules
import { OrderDocument, Order } from './order.schema';
import { UserDocument } from 'src/auth/user.schema';
import { GetOrdersFilterDTO } from './dto/order-filter.dto';
import { SortOrdersEnum } from './dto/sort-orders.enum';
import { OrderStatus } from './dto/order-status.enum';
import { CartItem, AddressBeforeEncryption } from 'src/auth/dto/interfaces';
import { ProductDocument } from 'src/products/product.schema';
import { CreateOrderDTO } from './dto/create-order.dto';
import { CheckOutDTO } from './dto/checkout.dto';
import { encrypt, decrypt } from 'src/shared/cipherDecipher';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ProductReturn } from 'src/products/dto/product-return.interface';
import { ProductsService } from 'src/products/products.service';
import { ChangeAddressDTO } from './dto/change-address.dto';
import { OrderReturn } from './dto/order-return.interface';
import { MessageDocument } from 'src/contact/message.schema';
import { ConversationDocument } from 'src/contact/conversation.schema';
import { Services } from 'src/contact/dto/services.enum';
import { SendOrderMessageDTO } from './dto/send-order-message.dto';
import { ContactService } from 'src/contact/contact.service';
import { AuthService } from 'src/auth/auth.service';

//config
paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': config.get("paypal_client"),
    'client_secret': config.get("paypal_secret")
});

@Injectable()
export class OrdersService {

    private readonly logger = new Logger("Orderservice");

    constructor(
        @InjectModel('Conversation') private conversationModel: Model<ConversationDocument>,
        @InjectModel('Order') private orderModel: Model<OrderDocument>,
        @InjectModel('Message') private messageModel: Model<MessageDocument>,
        @InjectModel('Product') private productModel: Model<ProductDocument>,
        @InjectModel('User') private userModel: Model<UserDocument>,
        private mailerService: MailerService,
        private productService: ProductsService) { }

    
    @Cron('0 0 */5 * * *')
    async cleanPendingOrders (): Promise<void> {

        try {
            this.logger.log(`Cleaning old orders with status "PENDING"...`);

            const olderThan: Date = new Date();
            olderThan.setDate(olderThan.getDate() - 1);

            const result = await this.orderModel.deleteMany({status: OrderStatus.PENDING, orderedAt: {
                $lt: olderThan.toISOString()
            }});
            this.logger.log(result);

        } catch (ex) {

            this.logger.error(`Failure to clean orders with status "PENDING", reason: ${ex}`, ex);
        }
    }

    async getOrders(getOrderFilterDTO: GetOrdersFilterDTO,
        user: UserDocument): Promise<OrderDocument[]> {

        const { sort, status, offset, product_id, ascending } = getOrderFilterDTO;

        const aggregatePipeline = [];

        const finalAscending = ascending === "true";

        aggregatePipeline.push({
            $match: {
                "user._id": user._id
            }
        });

        if (product_id) {

            aggregatePipeline.push({
                $match: {
                    products: {
                        $all: product_id.map(p_id => ({
                            $elemMatch: {
                                _id: Types.ObjectId(p_id)
                            }
                        }))
                    }
                }
            });
        }

        if (status) {
            aggregatePipeline.push({
                $match: {
                    status: {
                        $in: status
                    }
                }
            });
        }

        if (offset) {

            aggregatePipeline.push(

                {
                    $skip: offset
                }
            );
        }

        const sortAfter = sort === SortOrdersEnum.SUM ? "sum" : "orderedAt";

        aggregatePipeline.push({

            $sort: {[sortAfter]: finalAscending ? 1 : -1}
        });

        aggregatePipeline.push({ $limit: 20 },
            { "$lookup": {
                "from": 'products',
                "localField": "products._id",
                "foreignField": "_id",
                "as": "product_docs"
            }},
            {
                "$project": {
                  "product_docs.orders": 0
                }
            }
        );

        aggregatePipeline.push({
            $match: {
                status: {
                    $ne: OrderStatus.PENDING
                }
            }
        });

        const orders = await this.orderModel.aggregate(aggregatePipeline);

        return Promise.all(orders.map(async (order) => {

            if (order.address) order.address = JSON.parse(decrypt(order.address));
            order.payer_id = null;
            order.payment_id = null;

            await Promise.all(order.product_docs.map((p: ProductReturn, i: number) => {

                return new Promise(async (resolve, _) => {
                    
                    const hasOrderedReviewed = await this.productService.checkIfOrderedReviewed(p._id, user);

                    if (hasOrderedReviewed && hasOrderedReviewed.hasOrdered) p.hasOrdered = true;
                    if (hasOrderedReviewed && hasOrderedReviewed.hasReviewed) 
                        p.hasReviewed = hasOrderedReviewed.hasReviewed;

                    if (order.products[i].discountPriceAtOrder) {

                        p.discountPrice = order.products[i].discountPriceAtOrder;

                    } else {
                        delete p.discountPrice;
                    }

                    order.product_docs[i] = p;

                    return resolve();
                })
            }));

            return order;
        }));

    }

    async getSingleOrder(id: string, user: UserDocument): Promise<OrderReturn> {

        const order = await this.orderModel.findById(id)
        .populate({path: "products._id", model: "Product"}).exec();

        if (!order || order && order.user._id.toString() !== user._id.toString())
            throw new NotFoundException("Order not found");

        return OrdersService.orderDocToReturn(order);

    }

    async getOrderByUUID (id: string, user: UserDocument): Promise<OrderReturn> {

        const order = await this.orderModel.findOne({order_id: id, "user._id": user._id})
        .populate({path: "products._id", model: "Product"}).exec();
        if (!order) throw new NotFoundException("Order not found");

        return OrdersService.orderDocToReturn(order);
    }

    async createOrder(createOrderDTO: CreateOrderDTO, user: UserDocument): Promise<string | paypal.Link[]> {

        const { express, address } = createOrderDTO;

        let sum = 0;

        if (express) sum += 3.99;

        const order_id = uuid();

        const create_payment_json = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": `${AuthService.host}/orders/checkout/${order_id}`,
                "cancel_url": `${AuthService.host}/orders/cancel/${order_id}`
            },
            "transactions": [{
                "item_list": {

                    "items": user.shoppingCart.map((item: CartItem) => {

                        const { _id } = item;

                        const extractedItem = <ProductDocument>_id;

                        const price = (extractedItem.discountPrice ? extractedItem.discountPrice : extractedItem.price);

                        sum += (price * item.count);

                        return {
                            name: extractedItem.title,
                            sku: extractedItem.title,
                            price: price.toString(),
                            quantity: item.count,
                            currency: "EUR"
                        }
                    }),
                },
                "amount": {
                    "currency": "EUR",
                    "total": (Math.floor(sum*100)/100).toString()
                },
                "description": "Pastry order from Baker Jönsson"
            }]
        };

        if (express) {
            create_payment_json.transactions[0].item_list.items.push({
                name: "Express shipping",
                sku: "Express shipping",
                price: "3.99",
                quantity: 1,
                currency: "EUR"
            });
        }

        return new Promise((resolve, reject) => {
            paypal.payment.create(create_payment_json, async (error, payment) => {
                if (error) {
                    this.logger.error(`Paypal request failed. Reason: ${error}`, JSON.stringify(error));
                    reject(error);
                } else {

                    const approval_url = payment.links.filter(link => link.rel === 'approval_url');

                    const delieveryDate = new Date();

                    let numberOfDaysToAdd = 1;

                    if (!express) numberOfDaysToAdd += 1;

                    if (delieveryDate.getDay() === 6) {
                        numberOfDaysToAdd += 2;
                    } else if (delieveryDate.getDay() === 0) {
                        numberOfDaysToAdd += 1;
                    }

                    delieveryDate.setDate(delieveryDate.getDate() + numberOfDaysToAdd);
                    delieveryDate.setHours(15);

                    const order = new this.orderModel({
                        sum: (Math.floor(sum*100)/100),
                        products: user.shoppingCart.map(item => {
                            const { _id } = item;

                            const extractedItem = <ProductDocument>_id;

                            const objToReturn: CartItem = {
                                _id: extractedItem._id,
                                count: item.count
                            };

                            if (extractedItem.discountPrice) objToReturn.discountPriceAtOrder = extractedItem.discountPrice;

                            return objToReturn;
                        }),
                        status: OrderStatus.PENDING,
                        orderedAt: new Date().toISOString(),
                        estimatedDelievery: delieveryDate.toISOString(),
                        user: {
                            _id: user._id
                        },
                        order_id
                    });

                    if (express) order.express = true;

                    if (address) order.address = encrypt(JSON.stringify(address));

                    const result = await this.saveOrder(order);

                    user.orders.push(result._id);
                    
                    user.shoppingCart = [];

                    try {
                        await user.save();
                    } catch (ex) {
                        this.logger.error(`Saving user failed. Reason: ${ex}`, ex);
                        throw new InternalServerErrorException();
                    }

                    resolve(approval_url);
                }
            });
        });
    }

    async changeAddress(changeAddressDTO: ChangeAddressDTO, id: string, user: UserDocument): Promise<OrderReturn> {

        const { newAddress, useCustomAddress } = changeAddressDTO;

        if (useCustomAddress && !newAddress) throw new BadRequestException("New address missing");

        const order = await this.orderModel.findById(id);
        if (!order) throw new NotFoundException("Order not found");

        if (user._id.toString() !== order.user._id.toString() && !user.isAdmin)
            throw new ForbiddenException("You don't have the user rights to execute that operation");

        if (order.status !== OrderStatus.PROCESSING) 
            throw new BadRequestException("Delievery address can't be changed after order already has been dispatched");

        let decryptedAddress: AddressBeforeEncryption;

        if (useCustomAddress) {

            if (order.address) {
                decryptedAddress = JSON.parse(decrypt(<string>order.address));

                for (const key in decryptedAddress) {
                    if (newAddress[key]) decryptedAddress[key] = newAddress[key];
                }
            }   else {

                decryptedAddress = {...newAddress};
            }
            order.address = encrypt(JSON.stringify(decryptedAddress));
        } else {
            order.address = null;
        }

        const result = await (await this.saveOrder(order))
        .populate({path: 'products._id', model: 'Product'})
        .execPopulate();

        return {
            _id: result._id,
            product_docs: result.products.map(p => {

                const pr = <ProductDocument>p._id;
                return pr;
            }),
            address: useCustomAddress ? {...decryptedAddress} : null,
            sum: result.sum,
            order_id: result.order_id,
            express: result.express,
            products: order.products,
            user: result.user,
            orderedAt: result.orderedAt,
            estimatedDelievery: result.estimatedDelievery,
            status: result.status
        };
    }

    async checkOut(checkoutDTO: CheckOutDTO, id: string, user: UserDocument): Promise<OrderDocument> {

        const order = await this.orderModel.findOne({ order_id: id, "user._id": user._id, status: OrderStatus.PENDING });
        if (!order) throw new NotFoundException("Order not found");

        if (user._id.toString() !== order.user._id.toString() && !user.isAdmin)
            throw new ForbiddenException("You don't have the user rights to execute that operation");

        if (order.status === OrderStatus.CANCELLED) throw new BadRequestException("This order has been cancelled");

        const { payer_id, payment_id } = checkoutDTO;

        return new Promise((resolve, reject) => {
            paypal.payment.execute(payment_id, {payer_id}, async (error, _) => {

                if (error) {
                    this.logger.error(`Payment execution failed. Reason: ${JSON.stringify(error.response.details)}`, JSON.stringify(error));
                    return reject(new InternalServerErrorException());
                }
                else {
                    order.payment_id = payment_id;
                    order.payer_id = payer_id;
                    order.status = OrderStatus.PROCESSING;

                    const result = await this.saveOrder(order);

                    result.payer_id = null;
                    result.payment_id = null;
                    if (result.address) result.address = JSON.parse(decrypt(<string>result.address));

                    for (const item of result.products) {

                        const itemCasted = <ProductDocument>item._id;

                        const product = await this.productModel.findById(itemCasted._id);

                        product.stock -= item.count;
                        if (product.stock < 0) product.stock = 0;

                        product.orders.push(result._id);

                        await this.saveProduct(product);
                    }

                    //send a message about the order success
                    await this.sendOrderMessage({
                        user,
                        subject: `Order #${order.order_id} Confirmation`,
                        message: `Your order was completed successfully. See below for details`,
                        details: `&order:${order._id};`
                    }, order);

                    return resolve(result);
                }
            });
        });
    }

    async cancelOrder(id: string, user: UserDocument): Promise<SuccessfulOPDTO> {

        const order = await this.orderModel.findById(id);
        if(!order)  throw new NotFoundException("Order not found");

        if (user._id.toString() !== order.user._id.toString() && !user.isAdmin)
            throw new ForbiddenException("You don't have the user rights to execute that operation");

        if (order.status !== OrderStatus.PROCESSING) 
            throw new BadRequestException(`This order can't be cancelled: order#${order.order_id}`);

        for (const item of order.products) {

            const itemCasted = <ProductDocument>item._id;

            const product = await this.productModel.findById(itemCasted._id);

            product.stock += item.count;

            await this.saveProduct(product);
        }

        order.status = OrderStatus.CANCELLED;

        await this.saveOrder(order);

        //send a message about the order cancellation
        await this.sendOrderMessage({
            user,
            subject: `Cancellation of order #${order.order_id}`,
            message: `Order #${order.order_id} was cancelled. A refund will follow shortly. Details below`,
            details: `&order:${order._id}#cancelled;`

        }, order);

        return {
            success: true
        };
    }

    private static orderDocToReturn (order: OrderDocument): OrderReturn {

        const toReturnObj: OrderReturn = {
            _id: order._id,
            product_docs: order.products.map((p, i) => {

                const pr = <ProductDocument>p._id;

                if (p.discountPriceAtOrder) {

                   pr.discountPrice = p.discountPriceAtOrder;

                } else {
                    delete pr.discountPrice;
                }
                return pr;
            }),
            sum: order.sum,
            order_id: order.order_id,
            express: order.express,
            products: order.products,
            user: order.user,
            orderedAt: order.orderedAt,
            estimatedDelievery: order.estimatedDelievery,
            status: order.status
        };

        if (order.address) toReturnObj.address = JSON.parse(decrypt(<string>order.address));

        return toReturnObj;
    }

    private async sendOrderMessage (sendOrderMessageDTO: SendOrderMessageDTO, order: OrderDocument): Promise<boolean> {

        const { user, subject, message, details} = sendOrderMessageDTO;

        return new Promise(async (res, rej) => {

            try {

                const serviceName = Services.ORDERSERVICE;

                //send generic order message
                let conversation: ConversationDocument = await
                this.conversationModel.findOne({
                    "participants.id": {
                        $all: [
                            serviceName, user._id.toString()
                        ]
                    }
                });

                if (!conversation) {

                    conversation = new this.conversationModel({
                        reply: false,
                        messages: [],
                        participants: [
                            {
                                id: serviceName,
                                writing: null
                            },
                            {
                                id: user._id.toString(),
                                writing: null
                            }
                        ]
                    });
                }

                const messageDoc: MessageDocument = new this.messageModel({
                    from: {
                        _id: serviceName
                    },
                    to: {
                        _id: user._id.toString()
                    },
                    conversation: {
                        _id: conversation._id
                    },
                    subject,
                    content: `${message} \n${details}`
                });

                const resultMessage = await messageDoc.save();

                conversation.messages.push(resultMessage._id);
                conversation.lastMessage = new Date().toISOString();
                conversation.lastMessageSentBy = serviceName;

                await this.saveConversation(conversation);

                //TODO: send mail
                await ContactService.sendMailIfEmailOn({
                    conversation,
                    mailerService: this.mailerService,
                    sender: Services.ORDERSERVICE,
                    senderId: Services.ORDERSERVICE,
                    userModel: this.userModel,
                    customMessage: `Order #${order.order_id} was successfully ${order.status === OrderStatus.CANCELLED ? 
                    'cancelled' : 'made'}. ${order.status === OrderStatus.CANCELLED ? '€' + order.sum + ' will be refunded shortly.' :
                    'A total of €' + order.sum + ' was charged.'}`,
                    extra: `${AuthService.host}/orders/${order._id}`,
                    subject: `${subject} - Bagare Jönsson`
                });

                return res(true);

            } catch (_2) {

                return rej(false);
            }
        });
    }

    private async saveConversation (conversation: ConversationDocument): Promise<ConversationDocument> {

        try {
            return conversation.save();
        } catch (ex) {

            this.logger.error(`Saving conversation failed. Reason: ${ex}`, ex);
        }
    }

    private async saveProduct (product: ProductDocument): Promise<ProductDocument> {

        try {
            return product.save();
        }  catch (ex) {
            this.logger.error(`Saving product failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }

    private async saveOrder(order: OrderDocument): Promise<OrderDocument> {

        try {
            return order.save();
        } catch (ex) {
            this.logger.error(`Saving order failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }
}
