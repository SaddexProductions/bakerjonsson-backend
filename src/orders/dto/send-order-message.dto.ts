//local modules
import { UserDocument } from "src/auth/user.schema";

export interface SendOrderMessageDTO {

    user: UserDocument;
    message: string;
    details: string;
    subject: string;
}