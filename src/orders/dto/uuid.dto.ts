//npm modules
import { IsString, IsUUID } from "class-validator";

export class UuidDTO {

    @IsString()
    @IsUUID("all", {message: "Not a valid order Id"})
    uuid: string;
}