//properties to sort orders after when retrieving them
export enum SortOrdersEnum {
    DATE = "DATE",
    SUM = "SUM"
}