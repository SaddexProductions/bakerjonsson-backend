//npm modules
import { IsString, MaxLength } from "class-validator";

export class CheckOutDTO {

    @IsString()
    @MaxLength(50)
    payer_id: string;

    @IsString()
    @MaxLength(50)
    payment_id: string;
}