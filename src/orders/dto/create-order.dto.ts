//npm modules
import { IsBoolean, IsOptional, IsNotEmpty, IsObject } from "class-validator";
import { Type } from "class-transformer";

//local modules
import { AddressDTO } from "src/shared/address.dto";

export class CreateOrderDTO {

    @IsBoolean()
    @IsOptional()
    express: boolean;

    @IsObject()
    @IsNotEmpty()
    @IsOptional()
    @Type(() => AddressDTO)
    address?: AddressDTO;
}