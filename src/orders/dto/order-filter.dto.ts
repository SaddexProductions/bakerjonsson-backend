//npm modules
import { 
    IsString, 
    IsEnum, 
    IsNumber, 
    IsInt, 
    Min, 
    IsOptional,
    Matches, 
    IsArray
} from "class-validator";
import { Transform } from "class-transformer";

//local modules
import { OrderStatus } from "./order-status.enum";
import { SortOrdersEnum } from "./sort-orders.enum";
import { objectIdRegex } from "src/shared/objectid-regex";

export class GetOrdersFilterDTO {

    @IsArray()
    @IsString({each: true})
    @IsEnum(OrderStatus, 
        {message: "status must be PENDING, PROCESSING, DISPATCHED, DELIEVERED or CANCELLED", each: true}
    )
    @IsOptional()
    status?: OrderStatus[];

    @IsNumber()
    @IsInt()
    @Transform(value => Number(value))
    @Min(0)
    @IsOptional()
    offset?: number;

    @IsString()
    @IsEnum(SortOrdersEnum, 
        {message: "sort must be DATE or SUM"})
    sort?: SortOrdersEnum;

    @IsString()
    @Matches(/true|false/)
    @IsOptional()
    ascending?: string;
    
    @IsArray()
    @IsString({each: true})
    @Matches(objectIdRegex, {message: "Not a valid objectId", each: true})
    @IsOptional()
    product_id?: string[];


}