//local modules
import { CartItem } from "src/auth/dto/interfaces";
import { AddressDTO } from "src/shared/address.dto";
import { OrderStatus } from "./order-status.enum";
import { ProductReturn } from "src/products/dto/product-return.interface";

export interface OrderReturn {

    _id: string;
    products: CartItem[];
    address?: AddressDTO;
    express: boolean;
    sum: number;
    order_id: string;
    orderedAt: string;
    product_docs: ProductReturn[];
    estimatedDelievery: string;
    status: OrderStatus;
    user: {
        _id: string;
    }

}