//npm modules
import { ValidateNested, IsOptional, IsBoolean } from "class-validator";

//local modules
import { AddressDTO } from "src/shared/address.dto";
import { Type } from "class-transformer";

export class ChangeAddressDTO {

    @IsOptional()
    @ValidateNested()
    @Type(() => AddressDTO)
    newAddress?: AddressDTO;

    @IsBoolean()
    useCustomAddress: boolean;
}