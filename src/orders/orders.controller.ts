//npm modules
import { 
    Controller, 
    Post, 
    UseGuards, 
    Put, 
    Get, 
    Body, 
    ValidationPipe, 
    Query, 
    Param, 
    UsePipes} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Link } from 'paypal-rest-sdk';

//local modules
import { GetUser } from 'src/auth/get-user.decorator';
import { UserDocument } from 'src/auth/user.schema';
import { OrderDocument } from './order.schema';
import { OrdersService } from './orders.service';
import { GetOrdersFilterDTO } from './dto/order-filter.dto';
import { ObjectIdDTO } from 'src/shared/objectid.dto';
import { CreateOrderDTO } from './dto/create-order.dto';
import { CheckOutDTO } from './dto/checkout.dto';
import { UuidDTO } from './dto/uuid.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ChangeAddressDTO } from './dto/change-address.dto';
import { OrderReturn } from './dto/order-return.interface';

@Controller('orders')
export class OrdersController {

    constructor (private ordersService: OrdersService){}

    @Get('/')
    @UseGuards(AuthGuard())
    @UsePipes(new ValidationPipe({transform: true}))
    getOrders (
        @Query() getOrderFilterDTO: GetOrdersFilterDTO,
        @GetUser() user: UserDocument): Promise<OrderDocument[]> {
        return this.ordersService.getOrders(getOrderFilterDTO, user);
    }

    @Get('/:id')
    @UseGuards(AuthGuard())
    getSingleOrder (
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
        @GetUser() user: UserDocument): Promise<OrderReturn> {

        const { id } = objectIdDTO;
        
        return this.ordersService.getSingleOrder(id, user);
    }

    @Get('/getbyuuid/:uuid')
    @UseGuards(AuthGuard())
    getOrderByUUID (
        @Param(ValidationPipe) uuidDTO: UuidDTO,
        @GetUser() user: UserDocument
    ): Promise<OrderReturn> {

        const { uuid } = uuidDTO;

        return this.ordersService.getOrderByUUID(uuid, user);
    }

    @Post('/')
    @UseGuards(AuthGuard())
    createOrder (
        @Body(ValidationPipe) createOrderDTO: CreateOrderDTO,
        @GetUser() user: UserDocument): Promise<string | Link[]> {
        return this.ordersService.createOrder(createOrderDTO, user);
    }

    @Post('/checkout/:uuid')
    @UseGuards(AuthGuard())
    checkOut (
    @Body(ValidationPipe) checkoutDTO: CheckOutDTO,
    @Param(ValidationPipe) uuidDTO: UuidDTO,
    @GetUser() user: UserDocument): Promise<OrderDocument> {

        const { uuid } = uuidDTO;

        return this.ordersService.checkOut(checkoutDTO, uuid, user);
    }

    @Put('/changeaddress/:id')
    @UseGuards(AuthGuard())
    changeAddress (
        @Body(ValidationPipe) changeAddressDTO: ChangeAddressDTO,
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
        @GetUser() user: UserDocument
    ): Promise<OrderReturn> {

        const { id } = objectIdDTO;

        return this.ordersService.changeAddress(changeAddressDTO, id, user);
    }

    @Put('/cancel/:id')
    @UseGuards(AuthGuard())
    cancelOrder (
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
        @GetUser() user: UserDocument): Promise<SuccessfulOPDTO> {
        
        const { id } = objectIdDTO;
        
        return this.ordersService.cancelOrder(id, user);
    }
}
