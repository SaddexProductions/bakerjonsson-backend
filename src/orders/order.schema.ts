//npm modules
import { Document, SchemaTypes } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

//local modules
import { CartItem } from 'src/auth/dto/interfaces';
import { OrderStatus } from './dto/order-status.enum';
import { AddressDTO } from 'src/shared/address.dto';
import { ProductReturn } from 'src/products/dto/product-return.interface';

const {ObjectId} = SchemaTypes;

export type OrderDocument = Order & Document;

@Schema()
export class Order {

    @Prop({
        type: Number,
        required: true
    })
    sum: number;

    @Prop({type: Boolean})
    express?: boolean;

    @Prop([{
        _id: {
            type: ObjectId,
            ref: "Product",
            required: true
        },
        count: {
            type: Number,
            required: true,
            min: 0
        },
        discountPriceAtOrder: {
            type: Number,
            min: 0.1,
            max: 19
        }
    }])
    products: CartItem[];

    //not saved in the database
    product_docs: ProductReturn[];

    @Prop({maxlength: 400, type: String})
    address?: string | AddressDTO;

    @Prop({
        type: {
            _id: {
                type: ObjectId,
                required: true,
                ref: "User"
            }
        },
        required: true
    })
    user: {
        _id: string;
    }

    @Prop({
        type: Date,
        required: true
    })
    orderedAt: string;

    @Prop({
        type: Date,
        required: true
    })
    estimatedDelievery: string;

    @Prop({
        type: String,
        required: true,
        enum: Object.keys(OrderStatus)
        .map(key => OrderStatus[key]),
    })
    status: OrderStatus;

    //used locally
    @Prop({type: String})
    order_id?: string;

    @Prop({type: String})
    payer_id?: string;
    
    //paypal payment id
    @Prop({type: String})
    payment_id?: string;
}

export const OrderSchema = SchemaFactory.createForClass(Order);