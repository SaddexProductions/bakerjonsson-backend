//npm modules
import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';

//local modules
import { AppModule } from './app.module';

async function bootstrap() {

  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  const port = process.env.NODE_ENV === "development" ? 3000 : 6000;
  if (process.env.NODE_ENV === "development") app.enableCors();
  else app.setGlobalPrefix('api');
  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
