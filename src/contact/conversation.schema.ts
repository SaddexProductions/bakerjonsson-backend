//npm modules
import { Document, SchemaTypes, Types } from "mongoose";
import { Schema, SchemaFactory, Prop } from "@nestjs/mongoose";

//local modules
import { MessageDocument } from "./message.schema";

export type ConversationDocument = Conversation & Document;

@Schema({timestamps: true})
export class Conversation {

    @Prop([{
        type: SchemaTypes.ObjectId,
        ref: "Message"
    }])
    messages?: string[] | MessageDocument[] | Types.ObjectId[];

    @Prop([{
        id: {
            type: String,
            required: true
        },
        name: {
            type: String
        },
        lastTyping: {
            type: Date
        },
        sendEmail: {
            type: Boolean
        }
    }])
    participants: Participant[];

    @Prop({
        required: true,
        type: Boolean
    })
    reply: boolean;

    @Prop({
        type: Date
    })
    lastMessage: string;

    @Prop({
        type: String
    })
    lastMessageSentBy: string;

    createdAt: Date;

    updatedAt: Date;
}

export interface Participant {

    id: string;
    name?: string;
    lastTyping: string;
    sendEmail?: boolean;
}

export const ConversationSchema = SchemaFactory.createForClass(Conversation);