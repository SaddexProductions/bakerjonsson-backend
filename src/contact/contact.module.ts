//npm modules
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import * as config from 'config';

//local modules
import { ContactController } from './contact.controller';
import { ContactService } from './contact.service';
import { ReportSchema } from './report.schema';
import { ReviewSchema } from 'src/reviews/review.schema';
import { MessageSchema } from './message.schema';
import { UserSchema } from 'src/auth/user.schema';
import { ConversationSchema } from './conversation.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Message', schema: MessageSchema},
      {name: 'Report', schema: ReportSchema},
      {name: 'Review', schema: ReviewSchema},
      {name: 'User', schema: UserSchema},
      {name: 'Conversation', schema: ConversationSchema}
    ]),
    PassportModule.register({
      defaultStrategy: 'jwt'
    }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || config.get('baker_jwtPrivateKey'),
      signOptions: {
        expiresIn: (24 * 3600 * 30)
      }
    }),
  ],
  controllers: [ContactController],
  providers: [ContactService]
})
export class ContactModule {}
