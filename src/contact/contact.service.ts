//npm modules
import { Injectable, NotFoundException, Logger, BadRequestException, ForbiddenException } from '@nestjs/common';
import * as hb from 'handlebars';
import { MailerService } from '@nestjs-modules/mailer';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';


//local modules
import { ReportDocument, Report } from './report.schema';
import { PostReportDTO } from './dto/post-report.dto';
import { UserDocument } from 'src/auth/user.schema';
import { ReviewDocument } from 'src/reviews/review.schema';
import { readFileAsync } from 'src/shared/readFileAsync';
import { MessageDocument } from './message.schema';
import { MessageDTO } from './dto/message.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ConversationDocument, Participant } from './conversation.schema';
import { MarkAsRead, CountMessages } from './dto/contact.interfaces';
import { GetConversationsDTO } from './dto/get-conv.dto';
import { CheckConvDTO, CheckConvDTOInd } from './dto/check-conv.dto';
import { GetMessagesDTO } from './dto/get-messages.dto';
import { Services } from './dto/services.enum';
import { ReplyDTO } from './dto/reply.dto';
import { MarkAsReadDTO } from './dto/mark-as-read.dto';
import { objectIdRegex } from 'src/shared/objectid-regex';
import { AuthService } from 'src/auth/auth.service';
import { SetTypingDTO } from './dto/set-typing.dto';

@Injectable()
export class ContactService {

    private readonly logger = new Logger("Contactservice");

    constructor (
        @InjectModel('Conversation') private conversationModel: Model<ConversationDocument>,
        @InjectModel('Message') private messageModel: Model<MessageDocument>,
        @InjectModel('Report') private reportModel: Model<ReportDocument>,
        @InjectModel('Review') private reviewModel: Model<ReviewDocument>,
        @InjectModel('User') private userModel: Model<UserDocument>,
        private mailerService: MailerService,
    ) {}

    async getSingleReport (id: string): Promise<ReportDocument> {

        const report = await this.reportModel.findById(id);
        if (!report) throw new NotFoundException("Report not found");

        return report;
    }

    async report (postReportDTO: PostReportDTO, user: UserDocument): Promise<SuccessfulOPDTO> {

        const { id, email, reason, details, sendEmail } = postReportDTO;

        let finalEmail = sendEmail && email ? email : null;

        if (user) finalEmail = sendEmail ? user.email : null;

        const review = await this.reviewModel.findById(id)
        .populate({path: 'creator._id', model: 'User'}).exec();
        if (!review) throw new NotFoundException("The reported review was not found");

        const obj: Report = {
            reason,
            review: {
                _id: <any>Types.ObjectId(id)
            }
        }

        if (finalEmail) obj.email = finalEmail;
        if (details) obj.details = details;
        if (user) obj.user = {
            _id: user._id
        };

        const report = new this.reportModel({...obj});

        const creator = <UserDocument>review.creator._id;
        const name = `${creator.firstName} ${creator.lastName.charAt(0)}`;
        const subject = `Successfully reported review from ${name}`;
        const message = `You have reported ${name}'s review
        for "${reason}"`;
        const detailsLine = `You will recieve updates on this issue soon.`;

        if (finalEmail) {

            try {
                await ContactService.sendEmail({
                    email: finalEmail,
                    subject,
                    mailerService: this.mailerService,
                    message,
                    greeting: "",
                    details: detailsLine
                });
            } catch (ex) {

                this.logger.error(`Sending email failed. Reason: ${ex}`, ex);
                throw ex;
            }
        }

        if (user) {

            const serviceName = Services.REPORTSERVICE;

            const messageDoc: MessageDocument = new this.messageModel({
                from: {
                    _id: serviceName
                },
                to: {
                    _id: user._id
                },
                subject,
                content: `${message} - "${details} \nYou will recieve an update on this soon."`
            });

            let conversation = await this.conversationModel
            .findOne({"participants.id": {
                $all: [serviceName, user._id.toString()]
            }});
            if (!conversation) {

                conversation = new this.conversationModel({
                    participants: [{
                        id: serviceName,
                        name: serviceName,
                        lastTyping: null
                    }, {
                        id: user._id.toString(),
                        name: `${user.firstName} ${user.lastName}`,
                        lastTyping: null
                    }],
                    reply: false,
                    lastMessageSentBy: serviceName,
                    lastMessage: new Date().toISOString(),
                    messages: []
                });
            }

            messageDoc.conversation._id = conversation._id;

            const result = await this.saveMessage(messageDoc);

            conversation.messages.push(result._id);
            conversation.lastMessage = new Date().toISOString();
            conversation.lastMessageSentBy = serviceName;
            const conResult = await this.saveConversation(conversation);

            const userNotPopulated = await this.userModel.findById(user._id);

            userNotPopulated.conversations.push(conResult._id); 
            await userNotPopulated.save();
        }

        await report.save();

        return {
            success: true
        }
    }

    async setEmailNotice (conversation_id: string, user: UserDocument, value: boolean): Promise<SuccessfulOPDTO> {
        
        const conversation = await this.conversationModel
        .findOne({_id: conversation_id, "participants.id": {$in: user._id}});
        if (!conversation) throw new NotFoundException("Conversation not found");

        const participantWithIndex = 
        ContactService.getParticipantAndIndex(conversation.participants, user._id);

        participantWithIndex.participant.sendEmail = value;
        conversation.participants[participantWithIndex.index] = participantWithIndex.participant;

        await this.saveConversation(conversation);

        return {
            success: true
        };
    }

    async postMessage (messageDTO: MessageDTO, user: UserDocument): Promise<MessageDocument | SuccessfulOPDTO> {

        const { email, subject, content, sendEmail, sendAsAdmin, toUser } = messageDTO;

        let recUserExists: UserDocument;

        let ix: number = -1;

        if (!user && !email) throw new BadRequestException("Email is required when not signed in");

        if (user) {

            let conversation: ConversationDocument;

            if (sendAsAdmin && !user.isAdmin) {
                throw new ForbiddenException("You lack the user rights to perform this operation");
            }

            if (toUser && user.isAdmin && sendAsAdmin) {

                recUserExists = await this.userModel.findById(toUser);
                if (!recUserExists) throw new NotFoundException("Recipient does not exist");
            }

            conversation = await this.conversationModel.findOne({
                "participants.id": {
                    $all: [
                        "admin", user.isAdmin && sendAsAdmin && toUser ? toUser : user._id.toString()
                    ]
                }
            });

            if (!conversation) {

                conversation = new this.conversationModel({
                    reply: true,
                    messages: [],
                    lastMessageSentBy: user.isAdmin && sendAsAdmin ? Services.ADMIN : user._id.toString(),
                    lastMessage: new Date().toISOString(),
                    participants: [{
                            id: "admin",
                            name: "admin",
                            lastTyping: null
                        },
                        {
                            id: user._id.toString(),
                            name: `${user.firstName} ${user.lastName}`,
                            lastTyping: null,
                            sendEmail: !!sendEmail
                        }
                    ]
                });

                conversation = await this.saveConversation(conversation);
            } else {

                let searchId = user._id.toString();

                if (sendAsAdmin)
                    searchId = Services.ADMIN;
                
                ix = conversation.participants.findIndex(p => p.id === searchId);

                if (sendEmail && !sendAsAdmin)
                    conversation.participants[ix].sendEmail = true;
            }


            const message = new this.messageModel({
                subject,
                content,
                conversation: {
                    _id: conversation._id
                }
            });

            if (user.isAdmin && sendAsAdmin && toUser) {

                message.fromAdmin = true;
                message.from = {
                    _id: "admin"
                }
                message.to = {
                    _id: toUser
                }
                
            } else {
                message.toAdmin = true;
                message.from = {
                    _id: user._id
                },
                message.to = {
                    _id: Services.ADMIN
                }
            }

            const result = await this.saveMessage(message);

            conversation.messages.push(result._id);
            conversation.lastMessage = new Date().toISOString();
            if (ix > -1) conversation.participants[ix].lastTyping = null;
            const sender = sendAsAdmin && toUser ? "admin" : user.firstName;
            const senderId = sendAsAdmin && toUser ? "admin" : user._id.toString();
            conversation.lastMessageSentBy = senderId;

            //sendEmail
            await ContactService
            .sendMailIfEmailOn({
                sender,
                senderId,
                mailerService: this.mailerService,
                content,
                userModel: this.userModel,
                conversation,
                extra: `${AuthService.host}/messages/${conversation._id}`
            });

            const conResult = await this.saveConversation(conversation);

            const userToSave: UserDocument = sendAsAdmin ? recUserExists : user;

            const userConvs = <string[]>(userToSave.conversations);

            //Array of objectIds needs to be converted to plain strings
            const userConvsStringArray = userConvs.map(c => c.toString());

            if (userConvsStringArray.indexOf(conResult._id.toString()) === -1) {
                userToSave.conversations.push(conResult._id);
                await this.saveUser(userToSave);
            }

            return result;

        } else {

            await ContactService.sendEmail({
                email: "saddexproductions@gmail.com", 
                subject,
                greeting: "",
                mailerService: this.mailerService,
                message: content, 
                details: ""
            });

            return {
                success: true
            }
        }
    }

    async countAllUnread (user: UserDocument, unreadFromMe: string[]): Promise<CountMessages> {

        const unread = await this.messageModel.find({"to._id": user._id.toString(), read: false});

        const toReturn: CountMessages = {
            count: unread.length
        }

        if (unreadFromMe) {

            const nowReadFromMe = await this.messageModel
            .find({"from._id": user._id, read: true, _id: {$in: unreadFromMe}});

            if (nowReadFromMe.length) {

                toReturn.nowReadMessagesFromMe = nowReadFromMe.map(m => m._id);
            }
        }

        return toReturn;
    }

    async markAllAsRead (user: UserDocument, markAsReadDTO: MarkAsReadDTO): Promise<SuccessfulOPDTO> {

        const { id, asAdmin } = markAsReadDTO;

        const userId = asAdmin && user.isAdmin ? Services.ADMIN : user._id.toString();

        const filterObj: MarkAsRead = {
            "to._id": userId,
            read: false
        }

        let convIds: string[] = [];

        if (id) {

            await this.findConversation(userId, id);
            
            filterObj["conversation._id"] = Types.ObjectId(id);

            convIds.push(id);

        } else {

            const messages = await this.messageModel.find(filterObj);

            convIds = messages.map(m => m.conversation._id.toString());
        }

        const result = await this.messageModel.updateMany(filterObj, {$set: {
            read: true
        }});

        await this.conversationModel
        .updateMany({_id: {$in: convIds}}, {$set: {updatedAt: new Date()}});

        return {
            success: result && !!result.nModified
        };
    }

    async getConversations (getConversationsDTO: GetConversationsDTO, user: UserDocument): Promise<ConversationDocument[]> {

        let { skip, limit } = getConversationsDTO;

        if (!skip && skip !== 0) skip = 0;
        
        const result = await this.conversationModel.find({"participants.id": {$in: user._id}}).skip(skip).limit(limit).sort("-lastMessage")
        .populate({path: "messages", model: "Message", options: {
            sort: "-createdAt",
        }}).exec();

        ContactService.preprocessConversations(result);

        return result;
    }

    async getMessages (id: string, getMessagesDTO: GetMessagesDTO, user: UserDocument): Promise<MessageDocument[]> {

        let { skip, limit, newerThan } = getMessagesDTO;

        //guards messages against retrieval from users that do not participate in it
        const conversation = await this.findConversation(user._id, id);

        const match: {[key: string]: any} = {"conversation._id": conversation._id};

        if (newerThan) match.createdAt = {$gt: new Date(newerThan)};

        const aggregatePipeline: Object[] = [
            {$match: match},
            {$sort: {'createdAt': -1}},
        ];

        if (skip) aggregatePipeline.push({$skip: skip});
        aggregatePipeline.push({$limit: limit});

        return this.messageModel.aggregate(aggregatePipeline);
    }

    async reply (messages: ReplyDTO[], conv_id: string, user: UserDocument): Promise<MessageDocument[]> {

        const recipient = messages[0].to._id;

        const conversation = await this.findConversation([user._id.toString(), recipient], conv_id);
        if (!conversation.reply) throw new BadRequestException("You can't reply to this user");

        const messagesToInsert = messages.map((m, i) => {

            const { content, subject, to, localId } = m;

            if (to._id !== recipient) throw new BadRequestException("Recipients don't match");

            const date = new Date();
            date.setSeconds(date.getSeconds() + i);

            const toReturn = new this.messageModel({
                content,
                from: {
                    _id: user._id
                },
                to,
                conversation: {
                    _id: conversation._id
                },
                localId,
                createdAt: date,
                updatedAt: date
            });

            if (subject) toReturn.subject = subject;

            if (m.to._id === Services.ADMIN) toReturn.toAdmin = true;

            return toReturn;
        });

        const messageIds = messagesToInsert.map(m => m._id);

        const result = await this.messageModel.insertMany(messagesToInsert);
        
        conversation.messages = [
            ...conversation.messages,
            ...messageIds
        ];
        conversation.lastMessageSentBy = user._id.toString();
        conversation.lastMessage = new Date().toISOString();

        const participantWithIndex = 
        ContactService.getParticipantAndIndex(conversation.participants, user._id.toString());

        conversation.participants[participantWithIndex.index].lastTyping = null;

        //sendEmail
        await ContactService
        .sendMailIfEmailOn({
            sender: user.firstName,
            senderId: user._id.toString(),
            mailerService: this.mailerService,
            content: messages.length === 1 
            ? messages[0].content : `${messages[0].content} + ${messages.length - 1} other messages`,
            userModel: this.userModel,
            conversation
        });
        
        await this.saveConversation(conversation);

        return result;

    }

    static async sendMailIfEmailOn (
        
        input: {
            conversation: ConversationDocument;
            userModel: Model<UserDocument>;
            sender: string;
            senderId: string;
            content?: string;
            customMessage?: string;
            subject?: string;
            extra?: string;
            mailerService: MailerService;
        }): Promise<void> {

        let {
            extra,
            conversation,
            customMessage,
            sender, 
            userModel, 
            content, 
            mailerService, 
            subject,
            senderId
         } = input;

        const partsToMail = await Promise.all(conversation.participants.filter(
            p => p.sendEmail && p.id !== senderId && objectIdRegex.test(p.id)
        ).map(async (p) => {
            const userInPart = await userModel.findById(p.id);
            if (userInPart) return userInPart;
        }).filter(u => u));

        if (partsToMail.length) {

            if (!content) content = "";

            await Promise.all(partsToMail.map(async (u: UserDocument) => 

                ContactService.sendEmail({
                    email: u.email,
                    subject: subject ? subject : `New message from ${sender} - Bagare Jönsson`,
                    greeting: ` ${u.firstName}`,
                    message: customMessage ? customMessage : `You have recieved a new message from ${sender}.`,
                    details: `${content}`,
                    extra,
                    mailerService: mailerService
                })
            ));
        }
    }

    static async sendEmail (
        input: {
            email: string;
            subject: string;
            extra?: string;
            message: string;
            greeting: string;
            details: string;
            mailerService: MailerService
        }
    ): Promise<any> {

        const {
            email,
            subject,
            greeting,
            message, 
            details,
            extra,
            mailerService
         } = input;

        const html = await readFileAsync(`${process.env.NODE_ENV === "development" ? 'src' :
         '..'}/templates/contact-template.html`, { encoding: 'utf-8' })

        let template = hb.compile(html);

        const templateToSend = template({
            greeting,
            subject,
            message,
            details,
            extra
        });

        return mailerService.sendMail({
            to: email, // list of receivers
            subject, // Subject line
            html: templateToSend
        });
    }

    async checkIfConvChanged (checkConvDTO: CheckConvDTO, user: UserDocument): Promise<ConversationDocument[]> {

        checkConvDTO.conversationsToCheck.sort((a, b) => {
            if(a.updatedAt > b.updatedAt) { return -1; }
            if(a.updatedAt < b.updatedAt) { return 1; }
            return 0;
        });

        const mostRecentDate = new Date(checkConvDTO.conversationsToCheck[0].updatedAt);

        const ids = checkConvDTO.conversationsToCheck
        .map(c => c.id);

        const conversations = await this.conversationModel
        .find({$or: [{_id: {$in: ids}, "participants.id": {$in: user._id}},
            {createdAt: {$gt: mostRecentDate}, "participants.id": {$in: user._id}}
        ]}).populate({path: "messages", model: "Message", options: {
            sort: "-createdAt"
        }}).exec();

        ContactService.preprocessConversations(conversations);

        conversations.sort(this.idSort);

        const isChanged = conversations.filter((o, i) => {

            const c = checkConvDTO.conversationsToCheck
            .findIndex(c => c.id === o.id);

            return (c > -1 && conversations[i].id.toString() === checkConvDTO.conversationsToCheck[c].id.toString()
                && conversations[i].updatedAt.toISOString() !== checkConvDTO.conversationsToCheck[c].updatedAt);
        });

        return isChanged;
    }

    async setTyping (id: string, user: UserDocument, setTypingDTO: SetTypingDTO): Promise<SuccessfulOPDTO> {

        let userid = user._id;

        if (setTypingDTO.asAdmin && user.isAdmin) userid = Services.ADMIN;

        const conversation = await this.findConversation(userid, id);

        const participantWithIndex = 
        ContactService.getParticipantAndIndex(conversation.participants, userid);

        participantWithIndex.participant.lastTyping = new Date().toISOString();

        conversation.participants[participantWithIndex.index] = participantWithIndex.participant;
        await this.saveConversation(conversation);

        return {
            success: true
        };
    }

    private static preprocessConversations (conversations: ConversationDocument[]): void {

        conversations.map(r => {

            r.messages.length = 1;
        });
    }

    private async findConversation (parts_ids: string | string[], conv_id: string): Promise<ConversationDocument> {

        const conversation = await this.conversationModel.findOne({_id: Types.ObjectId(conv_id), "participants.id": {$in: parts_ids}});
        if (!conversation) throw new NotFoundException("Conversation not found");

        return conversation;
    }

    private idSort (a: ConversationDocument | CheckConvDTOInd, b: ConversationDocument | CheckConvDTOInd) {

        if(a.id < b.id) { return -1; }
        if(a.id > b.id) { return 1; }
        return 0;
    }

    private async saveConversation (conversation: ConversationDocument): Promise<ConversationDocument> {

        try {
            return conversation.save();
        } catch (ex) {

            this.logger.error(`Saving conversation failed. Reason: ${ex}`, ex);
        }
    }

    private async saveMessage (message: MessageDocument): Promise<MessageDocument> {

        try {
            return message.save();
        } catch (ex) {

            this.logger.error(`Saving message failed. Reason: ${ex}`, ex);
        }
    }

    private async saveUser (user: UserDocument) {
        
        try {
            return user.save();
        }  catch (ex) {
            this.logger.error("Save user failed. Reason: " + ex, ex);
        }
    }

    private static getParticipantAndIndex (participants: Participant[], user_id: string): 
    {participant: Participant; index: number} {

        let index = -1;

        const participant = participants.find((p, i) => {

            if (p.id === user_id.toString()) {

                index = i;
            }

            return p.id === user_id.toString();
        });

        return {
            index,
            participant
        }
    }
}
