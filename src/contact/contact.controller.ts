//npm modules
import { 
    Controller, 
    Get, 
    Param, 
    ValidationPipe, 
    Post, 
    Body, 
    UseGuards, 
    Query, 
    UsePipes, 
    Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

//local modules
import { ObjectIdDTO } from 'src/shared/objectid.dto';
import { GetUser } from 'src/auth/get-user.decorator';
import { ContactService } from './contact.service';
import { UserDocument } from 'src/auth/user.schema';
import { ReportDocument } from './report.schema';
import { PostReportDTO } from './dto/post-report.dto';
import { OptionalJwtAuthGuard } from 'src/auth/optional-auth.guard';
import { MessageDocument, Message } from './message.schema';
import { MessageDTO } from './dto/message.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { ConversationDocument } from './conversation.schema';
import { GetConversationsDTO } from './dto/get-conv.dto';
import { CheckConvDTO } from './dto/check-conv.dto';
import { GetMessagesDTO } from './dto/get-messages.dto';
import { CountMessages } from './dto/contact.interfaces';
import { UnreadMessagesDTO } from './dto/unread.dto';
import { BoolValueDTO } from 'src/shared/bool.dto';
import { ReplyArrayDTO } from './dto/reply.dto';
import { MarkAsReadDTO } from './dto/mark-as-read.dto';
import { SetTypingDTO } from './dto/set-typing.dto';

@Controller('contact')
export class ContactController {

    constructor (private contactService: ContactService) {}

    @Get('/report/:id')
    @UseGuards(AuthGuard())
    getReport (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @GetUser({adminRequired: true}) _: UserDocument): Promise<ReportDocument> {

        const { id } = objectIdDTO;

        return this.contactService.getSingleReport(id);
    }

    @Post('/report/')
    @UseGuards(OptionalJwtAuthGuard)
    postReport (
        @Body(ValidationPipe) postReportDTO: PostReportDTO, 
        @GetUser({ignoreError: true}) user: UserDocument): Promise<SuccessfulOPDTO> {

        return this.contactService.report(postReportDTO, user ? user : null);
    }

    @Post('/conversations/markallasread')
    @UseGuards(AuthGuard())
    markAllAsRead 
    (
    @GetUser() user: UserDocument, 
    @Body(ValidationPipe) markAsReadDTO: MarkAsReadDTO): Promise<SuccessfulOPDTO> {

        return this.contactService.markAllAsRead(user, markAsReadDTO);
    }

    @Post('/conversations/checkifchanged')
    @UseGuards(AuthGuard())
    checkIfChanged (
        @Body(ValidationPipe) checkConvDTO: CheckConvDTO,
        @GetUser() user: UserDocument
    ): Promise<ConversationDocument[]> {

        return this.contactService.checkIfConvChanged(checkConvDTO, user);
    }

    @Get('/conversations/:id')
    @UsePipes(new ValidationPipe({transform: true}))
    @UseGuards(AuthGuard())
    getMessages (
        @Param() objectIdDTO: ObjectIdDTO, 
        @Query() getMessagesDTO: GetMessagesDTO,
        @GetUser() user: UserDocument): Promise<MessageDocument[]> {

        const { id } = objectIdDTO;

        return this.contactService.getMessages(id, getMessagesDTO, user);
    }

    @Post('/conversations/settyping/:id')
    @UseGuards(AuthGuard())
    setTyping (
        @Body(ValidationPipe) setTypingDTO: SetTypingDTO,
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
        @GetUser() user: UserDocument)
    : Promise<SuccessfulOPDTO> {

        const { id } = objectIdDTO;

        return this.contactService.setTyping(id, user, setTypingDTO);
    }

    @Put('/conversations/:id')
    @UseGuards(AuthGuard())
    setSendEmailNotice (
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
        @GetUser() user: UserDocument,
        @Body(ValidationPipe) boolDTO: BoolValueDTO
    ): Promise<SuccessfulOPDTO> {

        const { id } = objectIdDTO;
        const { value } = boolDTO;

        return this.contactService.setEmailNotice(id, user, value);
    }

    @Get('/conversations')
    @UsePipes(new ValidationPipe({transform: true}))
    @UseGuards(AuthGuard())
    getConversations (
        @Query() getConversationsDTO: GetConversationsDTO,
        @GetUser() user: UserDocument,
    ): Promise<ConversationDocument[]> {

        return this.contactService.getConversations(getConversationsDTO, user);
    }

    @Post('/')
    @UseGuards(OptionalJwtAuthGuard)
    sendMessage (
        @Body(ValidationPipe) messageDTO: MessageDTO,
        @GetUser({ignoreError: true}) user: UserDocument
    ): Promise<MessageDocument | SuccessfulOPDTO> {
        
        return this.contactService.postMessage(messageDTO, user);
    }

    @Post('/countunread')
    @UseGuards(AuthGuard())
    countAllUnread (
    @Body(ValidationPipe) unreadDTO: UnreadMessagesDTO,
    @GetUser() user: UserDocument): Promise<CountMessages> {

        const { unread } = unreadDTO;

        return this.contactService.countAllUnread(user, unread);
    }

    @Post('/conversations/reply/:id')
    @UseGuards(AuthGuard())
    reply (
        @Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
        @Body(ValidationPipe) replyDTO: ReplyArrayDTO,
        @GetUser() user: UserDocument
    ): Promise<MessageDocument[]> {

        const { id } = objectIdDTO; 

        const { messages } = replyDTO;

        return this.contactService.reply(messages, id, user);
    }
}
