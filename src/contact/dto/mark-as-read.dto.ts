//npm modules
import { IsString, IsOptional, Matches, IsBoolean } from "class-validator";

//local modules
import { objectIdRegex } from "src/shared/objectid-regex";

export class MarkAsReadDTO {

    @IsString()
    @IsOptional()
    @Matches(objectIdRegex, {message: "Not a valid objectId"})
    id?: string;

    @IsBoolean()
    @IsOptional()
    asAdmin?: boolean;
}