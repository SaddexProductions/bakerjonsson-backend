//npm modules
import { IsOptional, IsNumber, IsInt, Min } from "class-validator";
import { Transform } from "class-transformer";

export class GetConversationsDTO {

    @IsNumber()
    @IsInt()
    @Transform(value => Number(value))
    @Min(0)
    limit: number;

    @IsNumber()
    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    @Min(1)
    skip?: number;
}