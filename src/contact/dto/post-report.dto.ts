//npm modules
import { 
    IsString, 
    IsOptional,
    MaxLength, 
    MinLength, 
    IsEmail,
    IsBoolean
} from "class-validator";

//local modules
import { ObjectIdDTO } from "src/shared/objectid.dto";

export class PostReportDTO extends ObjectIdDTO {

    @IsString()
    @MinLength(3)
    @MaxLength(50)
    reason: string;

    @IsString()
    @IsOptional()
    @MinLength(3)
    @MaxLength(150)
    details?: string;

    @IsBoolean()
    @IsOptional()
    sendEmail?: boolean;

    @IsString()
    @IsOptional()
    @IsEmail()
    email?: string;
}