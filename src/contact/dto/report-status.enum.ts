export enum ReportStatusEnum {
    PENDING = "PENDING",
    REJECTED = "REJECTED",
    SUCCESSFUL = "SUCCESSFUL"
}