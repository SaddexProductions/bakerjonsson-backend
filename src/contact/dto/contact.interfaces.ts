//npm modules
import { Types } from "mongoose";

//local modules
import { Count } from "src/shared/count.interface";

export interface MarkAsRead {

    read: boolean;
    "to._id": string | Types.ObjectId;
    "conversation._id"?: string | Types.ObjectId;
}

export interface CountMessages extends Count {

    nowReadMessagesFromMe?: string[];
}