//npm modules
import { IsDateString, IsArray, ValidateNested } from "class-validator";

//local modules
import { ObjectIdDTO } from "src/shared/objectid.dto";

export class CheckConvDTOInd extends ObjectIdDTO {

    @IsDateString()
    updatedAt: string;
}

export class CheckConvDTO {

    @IsArray()
    @ValidateNested({each: true})
    conversationsToCheck: CheckConvDTOInd[];
}