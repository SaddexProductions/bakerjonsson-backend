//npm modules
import { IsDateString, IsString, IsOptional } from "class-validator";

//local modules
import { GetConversationsDTO } from "./get-conv.dto";

export class GetMessagesDTO extends GetConversationsDTO {

    @IsString()
    @IsOptional()
    @IsDateString()
    newerThan?: string;
}