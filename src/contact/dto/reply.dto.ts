//npm modules
import { 
    IsString, 
    IsDateString, 
    IsOptional, 
    MinLength, 
    MaxLength, 
    IsUUID,
    ValidateNested,
    Matches
} from "class-validator";
import { Type } from "class-transformer";

//local modules
import { orderFilterRegex } from "./constants";

class EmbeddedObjectIdDTO {

    @IsString()
    @MaxLength(30)
    _id: string;
}

export class ReplyDTO {

    @IsString()
    @IsUUID()
    localId: string;

    @IsString()
    @MaxLength(300)
    @Matches(orderFilterRegex)
    content: string;

    @IsString()
    @MinLength(3)
    @IsOptional()
    @MaxLength(100)
    @Matches(orderFilterRegex)
    subject?: string;

    @Type(() => EmbeddedObjectIdDTO)
    to: EmbeddedObjectIdDTO;

    @IsString()
    @IsDateString()
    createdAt: string;

    @Type(() => EmbeddedObjectIdDTO)
    conversation: EmbeddedObjectIdDTO;
}

export class ReplyArrayDTO {

    @ValidateNested({ each: true })
    @Type(() => ReplyDTO)
    messages: ReplyDTO[];
}