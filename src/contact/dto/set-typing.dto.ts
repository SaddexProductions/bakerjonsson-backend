//npm modules
import { IsBoolean, IsOptional } from "class-validator";

export class SetTypingDTO {

    @IsBoolean()
    @IsOptional()
    asAdmin?: boolean;
}