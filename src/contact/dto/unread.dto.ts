//npm modules
import { IsString, IsArray, IsOptional, Matches } from "class-validator";
import { objectIdRegex } from "src/shared/objectid-regex";

export class UnreadMessagesDTO {

    @IsArray()
    @IsString({each: true})
    @Matches(objectIdRegex, {message: "Not a valid objectId", each: true})
    @IsOptional()
    unread?: string[];
}