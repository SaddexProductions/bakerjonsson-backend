//npm modules
import { IsString, IsOptional, Matches, MaxLength, IsEmail, IsBoolean, MinLength } from "class-validator";

//local modules
import { orderFilterRegex } from "./constants";
import { objectIdRegex } from "src/shared/objectid-regex";

export class MessageDTO {

    @IsString()
    @IsOptional()
    @Matches(objectIdRegex, {message: "Not a valid objectId"})
    toUser?: string;

    @IsString()
    @IsOptional()
    @MaxLength(200)
    @IsEmail()
    email?: string;

    @IsString()
    @MinLength(3)
    @IsOptional()
    @MaxLength(100)
    @Matches(orderFilterRegex)
    subject: string;

    @IsString()
    @MaxLength(300)
    @Matches(orderFilterRegex)
    content: string;

    @IsBoolean()
    @IsOptional()
    sendEmail?: boolean;

    @IsBoolean()
    @IsOptional()
    sendAsAdmin?: boolean;
}