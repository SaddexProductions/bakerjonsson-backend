//npm modules
import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes } from "mongoose";

//local modules
import { ReviewDocument } from "src/reviews/review.schema";
import { ReportStatusEnum } from "./dto/report-status.enum";
import { UserDocument } from "src/auth/user.schema";

export type ReportDocument = Report & Document;

@Schema({timestamps: true})
export class Report {

    @Prop({
        type: {
            id: {
                type: SchemaTypes.ObjectId,
                ref: "Review",
                required: true
            }
        },
        required: true
    })
    review: {
        _id: string | ReviewDocument;
    }

    @Prop({
        type: {
            _id: {
                type: SchemaTypes.ObjectId,
                ref: "User"
            }
        }
    })
    user?: {
        _id: string | UserDocument;
    }

    @Prop({
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    })
    reason: string;

    @Prop({
        type: String,
        minlength: 3,
        maxlength: 150
    })
    details?: string;

    @Prop({
        type: String,
        maxlength: 200
    })
    email?: string;

    @Prop({type: Boolean, default: false})
    resolved?: boolean;

    @Prop({
        type: String,
        default: ReportStatusEnum.PENDING,
        enum: Object.keys(ReportStatusEnum)
        .map(key => ReportStatusEnum[key])
    })
    status?: ReportStatusEnum;
}

export const ReportSchema = SchemaFactory.createForClass(Report);