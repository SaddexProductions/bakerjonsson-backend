//npm modules
import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes, Types } from "mongoose";

export type MessageDocument = Message & Document;

@Schema({timestamps: true})
export class Message {

    @Prop({
        type: {
            _id: {
                type: SchemaTypes.ObjectId,
                required: true,
                ref: "Conversation"
            }
        },
        required: true
    })
    conversation: {
        _id: string | Types.ObjectId;
    }

    @Prop({
        type: {
            _id: {
                type: String,
                required: true
            }
        },
        required: true
    })
    from: {
        _id: string;
    }

    //currently not used, may be used in the future
    @Prop({type: String})
    fromFull?: string;

    @Prop({
        type: Boolean
    })
    fromAdmin?: boolean;

    @Prop({
        type: {
            _id: {
                type: String,
                required: true
            }
        },
        required: true
    })
    to: {
        _id: string;
    }

    @Prop({
        type: Boolean
    })
    toAdmin?: boolean;

    @Prop({type: String, minlength: 3, maxlength: 100})
    subject?: string;

    @Prop({
        type: String,
        maxlength: 500,
        required: true
    })
    content: string;

    @Prop({
        type: Boolean,
        default: false
    })
    read: boolean;

    @Prop({
        type: String
    })
    localId?: string;

    @Prop({type: Date}) //manual override required for reply
    createdAt: string;

    @Prop({type: Date}) //manual override required for reply
    updatedAt: string;
}

export const MessageSchema = SchemaFactory.createForClass(Message);