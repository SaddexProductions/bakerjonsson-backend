//npm modules
import { Injectable } from '@nestjs/common';
import * as GeoIP from 'geoip-lite';
import * as config from 'config';

//local modules
import { Country } from './dto/country.interface';
import * as svgs from './dto/svgs.json';

@Injectable()
export class CountriesService {

    getCountry (ip: string): Country {

        let ipToUse = ip;

        if (process.env.NODE_ENV === "development") {

            ipToUse = config.get("localip");
        }

        const {country} = GeoIP.lookup(ipToUse);

        return {
            code: country.toLowerCase()
        }
    }

    getCountriesJSON (): Object {

        return svgs;
    }
}
