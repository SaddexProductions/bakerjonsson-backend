//npm modules
import { Controller, Get, Headers } from '@nestjs/common';

//local modules
import { CountriesService } from './countries.service';
import { Country } from './dto/country.interface';

@Controller('countries')
export class CountriesController {

    constructor(private countriesService: CountriesService) {}

    @Get('/allcountries')
    getCountriesJSON (): Object {
        
        return this.countriesService.getCountriesJSON();
    }

    @Get('/')
    getCountry (@Headers() headers: {ip: string; 'x-real-ip': string, header: Object}): Country {

        const ipToCheck = headers['x-real-ip'];

        return this.countriesService.getCountry(ipToCheck ? ipToCheck : "");
    }
}
