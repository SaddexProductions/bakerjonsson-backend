//npm modules
import { IsString, Matches } from "class-validator";

//local modules
import { objectIdRegex } from "./objectid-regex";

export class ObjectIdDTO {

    @IsString()
    @Matches(objectIdRegex, {message: "Not a valid objectId"})
    id: string;
}