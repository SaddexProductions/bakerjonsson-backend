//npm modules
import { IsBoolean } from "class-validator";

export class BoolValueDTO {

    @IsBoolean()
    value: boolean;
}