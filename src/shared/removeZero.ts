//removes the first digit from a phone number if it's a zero
export const removeZero = (cellphone: string): string => {
    const transformedPhone = cellphone.split('');
    if (transformedPhone[0] === "0") {
        transformedPhone.splice(0, 1)
    }
    return transformedPhone.join('');
}