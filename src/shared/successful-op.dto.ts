export interface SuccessfulOPDTO {
    success: boolean;
    rejectedChange?: boolean;
}