//MongoDB objectId format
export const objectIdRegex: RegExp = /^[a-f\d]{24}$/i;