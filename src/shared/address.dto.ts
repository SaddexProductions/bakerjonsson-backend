//npm modules
import { IsString, MinLength, MaxLength, IsOptional, Matches } from "class-validator";

//local modules
import { nameRegex, countryRegex } from "src/auth/miscConstants";

export class AddressDTO {
    
    @IsString()
    @MinLength(4)
    @MaxLength(200)
    street: string;

    @IsString()
    @MaxLength(50)
    @IsOptional()
    additional?: string;

    @IsString()
    @MaxLength(10)
    @MinLength(4)
    @Matches(/[0-9]{4,10}$/)
    zip: string;

    @IsString()
    @MaxLength(200)
    @Matches(nameRegex)
    city: string;

    @IsString()
    @MaxLength(200)
    @IsOptional()
    @Matches(nameRegex)
    province?: string;

    @IsString()
    @MinLength(2)
    @Matches(countryRegex)
    countryCode?: string;

}