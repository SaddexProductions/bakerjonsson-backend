//local modules
import { CartItem } from "src/auth/dto/interfaces";
import { ProductDocument } from "src/products/product.schema";

export const clearProductOrders = (shoppingCart: CartItem[]): CartItem[] => {
    return shoppingCart.map(item => {

        const castedItem = <ProductDocument>item._id;

        castedItem.orders = null;

        item._id = castedItem;
        return item;
    })
}