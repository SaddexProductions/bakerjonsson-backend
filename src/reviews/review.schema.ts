//npm modules
import { Document, SchemaTypes } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

//local modules
import { ProductDocument } from 'src/products/product.schema';
import { UserDocument } from 'src/auth/user.schema';

const {ObjectId} = SchemaTypes;

export type ReviewDocument = Review & Document;

@Schema()
export class Review {

    @Prop({
        type: {
            name: {
                type: String
            },
            _id: {
                type: ObjectId,
                required: true,
                ref: "User"
            }
        }
    })
    creator: {
        name?: string;
        _id: string | UserDocument;
    };

    @Prop({
        type: {
            _id: {
                type: ObjectId,
                required: true,
                ref: 'Product'
            },
            title: {
                type: String,
                required: true
            }
        }
    })
    product: {
        _id: string | ProductDocument;
        title: string;
    };

    @Prop({
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    })
    title: string;

    @Prop({
        type: Number,
        min: 1,
        max: 5,
        required: true
    })
    rating: number;

    @Prop({
        type: String,
        minlength: 10,
        maxlength: 500
    })
    content?: string;

    //not returned to user
    @Prop([{
        _id: {
            type: ObjectId,
            ref: "User",
        },
        positive: Boolean
    }])
    votes?: {
        _id: string;
        positive: boolean;
    }[];

    @Prop({type: Number, default: 0})
    voteSum: number;

    @Prop({type: Date, default: Date.now})
    createdAt?: string;

    @Prop({type: Date, default: Date.now})
    updatedAt?: string;

    //not saved in the database but required to manipulate the retrieved doc
    @Prop({
        type: {
            positive: Boolean
        }
    })
    hasVoted?: {
        positive: boolean;
    };
}

export const ReviewSchema = SchemaFactory.createForClass(Review);