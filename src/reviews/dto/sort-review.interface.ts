export interface SortReviewInterface {
    createdAt?: number;
    voteSum?: number;
    rating?: number;
}