//npm modules
import { 
    IsString, 
    MinLength, 
    MaxLength, 
    IsNumber, 
    IsInt, 
    Min, 
    Max, 
    IsOptional
} from "class-validator";

export class ReviewDTO {

    @IsString()
    @MinLength(5)
    @MaxLength(50)
    title: string;

    @IsNumber()
    @IsInt()
    @Min(1)
    @Max(5)
    rating: number;

    @IsString()
    @IsOptional()
    @MinLength(10)
    @MaxLength(500)
    content?: string;
}