//properties to sort reviews after when retrieving them
export enum SortReviewAfter {
    VOTES = "VOTES",
    DATE_POSTED = "DATE_POSTED",
    SCORE = "SCORE"
}