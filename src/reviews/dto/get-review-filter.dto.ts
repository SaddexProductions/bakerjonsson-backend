//npm modules
import { IsBoolean, IsString, IsEnum, IsNumber, IsInt, IsOptional, Matches } from "class-validator";
import { Transform } from "class-transformer";

//local modules
import { SortReviewAfter } from "./sort-review.enum";

export class GetReviewsFilterDTO {

    @IsString()
    @IsEnum(SortReviewAfter, {message: "Value must be uppercase and either 'VOTES', 'DATE_POSTED' or 'SCORE"})
    sortAfter: SortReviewAfter;

    @IsString()
    @Matches(/true|false/)
    ascending: string;

    @IsNumber()
    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    skip?: string;

    @IsNumber()
    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    limit?: string;
}