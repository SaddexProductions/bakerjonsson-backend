export interface VoteInterface {
    voteSum: number;
    positive?: boolean;
}