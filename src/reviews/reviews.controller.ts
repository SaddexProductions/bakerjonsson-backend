//npm modules
import { Controller, 
    Get, 
    Param, 
    ValidationPipe, 
    Post, 
    Put, 
    UseGuards, 
    Delete, 
    Body, 
    BadRequestException, 
    Query} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

//local modules
import { ObjectIdDTO } from 'src/shared/objectid.dto';
import { ReviewDocument } from './review.schema';
import { GetUser } from 'src/auth/get-user.decorator';
import { UserDocument } from 'src/auth/user.schema';
import { ReviewsService } from './reviews.service';
import { ReviewDTO } from './dto/review.dto';
import { GetReviewsFilterDTO } from './dto/get-review-filter.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { OptionalJwtAuthGuard } from 'src/auth/optional-auth.guard';
import { VoteInterface } from './dto/vote-return.interface';

@Controller('reviews')
export class ReviewsController {

    constructor(private reviewsService: ReviewsService){}

    @Get('/product/:id/')
    @UseGuards(OptionalJwtAuthGuard)
    getAllReviewsForProduct (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @Query(ValidationPipe) getReviewFilterDTO: GetReviewsFilterDTO,
    @GetUser({ignoreError: true}) user: UserDocument
    ): Promise<ReviewDocument[]> {

        const {id} = objectIdDTO;
        return this.reviewsService.getAllReviews(id, getReviewFilterDTO, user);
    }

    @Get('/:id')
    @UseGuards(OptionalJwtAuthGuard)
    getReview (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @GetUser({ignoreError: true}) user: UserDocument): Promise<ReviewDocument> {
        
        const {id} = objectIdDTO;
        return this.reviewsService.getReview(id, user);
    }

    @Get('/')
    @UseGuards(AuthGuard())
    getUserReviews (@Query(ValidationPipe) getReviewFilterDTO: GetReviewsFilterDTO,
    @GetUser() user: UserDocument): Promise<ReviewDocument[]> {

        return this.reviewsService.getAllReviews(user._id.toString(), getReviewFilterDTO, user, true);
    }

    @Post('/product/:id')
    @UseGuards(AuthGuard())
    postReview (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument,
    @Body(ValidationPipe) reviewDTO: ReviewDTO): Promise<ReviewDocument> {

        const {id} = objectIdDTO;
        return this.reviewsService.postReview(id, user, reviewDTO);
    }

    @Put('/:id')
    @UseGuards(AuthGuard())
    editReview (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument,
    @Body(ValidationPipe) reviewDTO: ReviewDTO): Promise<ReviewDocument> {

        const {id} = objectIdDTO;
        return this.reviewsService.editReview(id, user, reviewDTO);
    }

    @Delete('/:id')
    @UseGuards(AuthGuard())
    deleteReview (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument): Promise<SuccessfulOPDTO> {

        const {id} = objectIdDTO;
        return this.reviewsService.deleteReview(id, user);
    }

    @Post('/:id/vote')
    @UseGuards(AuthGuard())
    voteOnReview (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO, 
    @GetUser() user: UserDocument, 
    @Body('positive') positive: boolean): Promise<VoteInterface> {

        const { id } = objectIdDTO;
        if (typeof positive !== "boolean") throw new BadRequestException("'positive' must be of type 'boolean'");

        return this.reviewsService.voteOnReview(id, user, positive);
    }

    @Delete('/:id/vote')
    @UseGuards(AuthGuard())
    deleteVote (@Param(ValidationPipe) objectIdDTO: ObjectIdDTO,
    @GetUser() user: UserDocument): Promise<VoteInterface> {

        const { id } = objectIdDTO;

        return this.reviewsService.deleteVote(id, user);
    }
}
