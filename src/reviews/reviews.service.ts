//npm modules
import { Injectable, 
    NotFoundException, 
    BadRequestException, 
    ForbiddenException, 
    Logger, 
    InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

//local modules
import { ReviewDocument } from './review.schema';
import { UserDocument } from 'src/auth/user.schema';
import { ProductDocument } from 'src/products/product.schema';
import { ReviewDTO } from './dto/review.dto';
import { OrderDocument } from 'src/orders/order.schema';
import { OrderStatus } from 'src/orders/dto/order-status.enum';
import { GetReviewsFilterDTO } from './dto/get-review-filter.dto';
import { SuccessfulOPDTO } from 'src/shared/successful-op.dto';
import { SortReviewAfter } from './dto/sort-review.enum';
import { SortReviewInterface } from './dto/sort-review.interface';
import { VoteInterface } from './dto/vote-return.interface';

@Injectable()
export class ReviewsService {

    private readonly logger = new Logger("Reviewsservice");

    constructor (@InjectModel("Review") private reviewModel: Model<ReviewDocument>,
    @InjectModel("Product") private productModel: Model<ProductDocument>,
    @InjectModel("User") private userModel: Model<UserDocument>) {
    }

    async getAllReviews (id: string, 
        getReviewsFilterDTO: GetReviewsFilterDTO,
        user: UserDocument, getByUser?: boolean): Promise<ReviewDocument[]> {

        if (!getByUser) {

            const productExists = await this.productModel.findById(id);
            if (!productExists) throw new NotFoundException("Product not found");
        }

        const { ascending, skip, sortAfter, limit } = getReviewsFilterDTO;

        const ascendingFinal = ascending === "true";
        
        const skipFinal = +skip || 0;
        const limitFinal = +limit || 20;
        let sort: SortReviewInterface = {};

        switch (sortAfter) {

            case (SortReviewAfter.DATE_POSTED):
                sort = {createdAt: ascendingFinal ? 1 : -1};
                break;
            case (SortReviewAfter.SCORE):
                sort = {rating: ascendingFinal ? 1 : -1};
                break;
            default:
                sort = {voteSum: ascendingFinal ? 1 : -1};
        }

        let findBy;

        if (getByUser) {
            findBy = {"creator._id": Types.ObjectId(id)}
        } else {
            findBy = {"product._id": Types.ObjectId(id)};
        }

        const reviews = await this.reviewModel.find(findBy)
        .sort(sort).skip(skipFinal).limit(limitFinal)
        .populate({path: "creator._id", model: "User"}).exec();

        return reviews.map(review => {

            const castedUser = <UserDocument>review.creator._id;

            review.creator = {
                _id: castedUser._id,
                name: `${castedUser.firstName} ${castedUser.lastName.charAt(0)}`
            };
            
            if (user) {
                const hasVoted = review.votes.filter(v => v._id.toString() === user._id.toString());
                if (hasVoted && hasVoted.length > 0) review.hasVoted = {
                    positive: hasVoted[0].positive
                };
            }

            review.voteSum = this.calculateVotingScore(review);
            review.votes = null;

            return review;
        });
    }

    async getReview (id: string, user: UserDocument): Promise<ReviewDocument> {

        const review = await this.reviewModel.findById(id).populate({path: "product._id", model: "Product"}).exec();
        if (!review) throw new NotFoundException("Review not found");

        review.voteSum = this.calculateVotingScore(review);
        
        const productIdCastedToUnknown = <unknown>review.product._id;
        const castedProduct = <ProductDocument>productIdCastedToUnknown;

        castedProduct.orders = null;
        review.product._id = castedProduct;

        const creator = await this.userModel.findById(review.creator._id);
        review.creator.name = `${creator.firstName} ${creator.lastName.charAt(0)}`;

        if (user) {
            const hasVoted = review.votes.filter(v => v._id.toString() === user._id.toString());
            if (hasVoted && hasVoted.length > 0) review.hasVoted = {
                positive: hasVoted[0].positive
            };
        }

        review.votes = null;

        return review;
    }

    async postReview (id: string, user: UserDocument, reviewDTO: ReviewDTO): Promise<ReviewDocument> {

        const product = await this.productModel.findById(id).populate({path: "orders", model: "Order"}).exec();
        if (!product) throw new NotFoundException("Product not found");

        const reviewExists = await this.reviewModel.findOne({"creator._id": user._id, "product._id": product._id});
        if (reviewExists) throw new BadRequestException("You have already posted a review for this product");

        //check if user has bought this product, throws exception if not

        let orderExists: boolean = false;

        if (product.orders.length > 0 && typeof product.orders[0] !== "string") {

            const orders = <OrderDocument[]>product.orders;

            orderExists = orders.filter((order: OrderDocument) => 
            order.user._id.toString() === user._id.toString() && order.status === OrderStatus.DELIEVERED
            )
            .length > 0;

        }

        if (!orderExists) 
                throw new BadRequestException("You can only review this product if you have successfully ordered it");

        const review = new this.reviewModel({
            ...reviewDTO,
            creator: {
                _id: user._id
            },
            product: {
                title: product.title,
                _id: product._id
            },
            votes: []
        });

        const result = await this.saveReview(review);
        
        product.reviews.push(result._id);
        const newProduct = await (await this.saveProduct(product)).populate({path: 'reviews', model: "Review"})
        .execPopulate();

        user.reviews.push(result._id);
        await this.saveUser(user);

        await this.updateScore(newProduct);

        result.votes = null;
        result.creator.name = `${user.firstName} ${user.lastName.charAt(0)}`;

        return result;
    }

    async editReview (id: string, user: UserDocument, reviewDTO: ReviewDTO): Promise<ReviewDocument> {

        const { title, content, rating } = reviewDTO;

        const review = await this.reviewModel.findById(id);
        if (!review) throw new NotFoundException("Review not found");;

        if (user._id.toString() !== review.creator._id.toString()) 
            throw new ForbiddenException("You can't edit a review made by another user");

        review.rating = rating;
        review.title = title;
        review.content = content;
        review.updatedAt = new Date().toISOString();
        
        const result = await this.saveReview(review);

        result.creator.name = `${user.firstName} ${user.lastName.charAt(0)}`;

        const product = await this.productModel.findById(review.product._id)
        .populate({path: 'reviews', model: "Review"}).exec();

        await this.updateScore(product);

        result.votes = null;

        return result;
    }

    async deleteReview (id: string, user: UserDocument): Promise<SuccessfulOPDTO> {

        const review = await this.reviewModel.findById(id);
        if (!review) throw new NotFoundException("Review not found");

        const product = await this.productModel.findById(review.product._id);
        const reviewIds = (<string[]>product.reviews).map(s => s.toString());

        const reviewIndex = reviewIds.indexOf(id);

        if (user._id.toString() !== review.creator._id.toString() && !user.isAdmin) 
            throw new ForbiddenException("You can't delete a review made by another user");

        try {
            await review.remove();
        }  catch (ex) {
            this.logger.error(`Removing review failed. Reason: ${ex}` , ex);
            throw new InternalServerErrorException();
        }
        product.reviews.splice(reviewIndex, 1);
        const productResult = await (await this.saveProduct(product)).populate({path: "reviews",
        model: "Review"}).execPopulate();

        await this.updateScore(productResult);
        
        let userReviewIndex: number = -1;
        userReviewIndex = user.reviews.findIndex(r => r === id);
        if (userReviewIndex > -1) {

            user.reviews.splice(userReviewIndex, 1);
            await this.saveUser(user);
        }

        return {
            success: true
        }
    }

    async voteOnReview (id: string, user: UserDocument, value: boolean): Promise<VoteInterface> {

        const review = await this.reviewModel.findById(id);
        if (!review) throw new NotFoundException("Review not found");

        if (user._id.toString() === review.creator._id.toString()) 
            throw new BadRequestException("You can't vote on your own review");

        const voteIndex = this.getVoteIndex(review, user._id.toString());

        if (voteIndex > -1) {

            review.votes[voteIndex].positive = value;

        } else {
            review.votes.push({
                _id: user._id,
                positive: value
            });
        }

        review.voteSum = this.calculateVotingScore(review);

        const result = await this.saveReview(review);

        return {
            voteSum: result.voteSum,
            positive: value
        };
    }

    async deleteVote (id: string, user: UserDocument): Promise<VoteInterface> {

        const review = await this.reviewModel.findById(id);
        if (!review) throw new NotFoundException("Review not found");

        const voteIndex = this.getVoteIndex(review, user._id);
        if (voteIndex === -1) throw new NotFoundException("Vote not found");

        review.votes.splice(voteIndex, 1);
        review.voteSum = this.calculateVotingScore(review);
        const result = await this.saveReview(review);

        return {
            voteSum: result.voteSum
        };
    }

    private async saveProduct (product: ProductDocument): Promise<ProductDocument> {
        try {
            return product.save();
        }  catch (ex) {
            this.logger.error(`Saving product failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }

    private async saveReview (review: ReviewDocument): Promise<ReviewDocument> {

        try {
            return review.save();
        }  catch (ex) {
            this.logger.error(`Saving review failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }

    private async saveUser (user: UserDocument): Promise<UserDocument> {

        try {
            return user.save();
        }  catch (ex) {
            this.logger.error(`Saving user failed. Reason: ${ex}`, ex);
            throw new InternalServerErrorException();
        }
    }

    private getVoteIndex (review: ReviewDocument, _id:string): number {
        let voteIndex = -1;

        review.votes.map((vote, index) => {

            if (vote._id.toString() === _id.toString()) voteIndex = index; 
        });

        return voteIndex;
    }

    private calculateVotingScore (review: ReviewDocument): number {

        const positiveVotes = review.votes.filter(vote => vote.positive).length;
        const negativeVotes = review.votes.length - positiveVotes;

        return positiveVotes - negativeVotes;
    }

    private async updateScore (product: ProductDocument): Promise<void> {

        const {reviews} = product;

        if (reviews.length > 0) {

            const reviewArrayCasted = <unknown[]>reviews;

            let totalScore: number = 0;

            reviewArrayCasted.map((obj: ReviewDocument) => {

                totalScore += obj.rating;
            });

            product.averageRating = (totalScore/reviews.length);
        }   else {
            product.averageRating = null;
        }
        await this.saveProduct(product);
    }
}
